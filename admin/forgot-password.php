<?php
define('_SMARTY_STARTED', TRUE);
define('_ADMIN_STARTED', TRUE);

require_once dirname(dirname(__FILE__)) . '/config/config.php';

if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
	SMRedirect::go($config['BASE_URL'] . '/admin/accounts/');
}

$msg = NULL;
if (isset($_POST['submit_login'])) {
    $email = trim($_POST['email']);
	
    if (empty($email) ) {
        $msg = '<div class="alert alert-danger" role="alert">Please enter your email!</div>';
    } else {
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `email` = '$email' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) 
			$num = $rs->numrows();
		else 
			$num = 0;
		if ( $num == 1 ) {
			$user = $rs->getrows();
			$status = $user[0]['status'];
			if ( $status == 1 ) {
				// Auto generated random password
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
				$password = substr(str_shuffle($chars), 0, 8);
				
				$sha1_password = sha1($password);
				$queryU = "UPDATE `" . $config['db_prefix'] . "users` SET `password` = '$sha1_password' WHERE `email` = '$email'";
				$rsU = $conn->execute($queryU);
				if ( $rsU ) {
					//Email sending code
					$subject = 'New Login Credentials from ' . $config['site_name'];
					
					$message  = '<p>Hi ' . $user[0]['name'] . ',</p><br />';
					$message .= '<p>You have requested to reset your password.</p>';
					$message .= '<p>Now you can login with the following credentials:</p>';
					$message .= '<p>Below is your new login credentials:</p>';
					$message .= '<p>Username/Email: <strong>' . $email . '</strong></p>';
					$message .= '<p>Password: <strong>' . $password . '</strong></p>';
					$message .= '<p>Please keep this info safe and secure.</p><br />';
					$message .= '<p>You can login from the below link:</p>';
					$message .= '<p><a href="' . $config['BASE_URL'] . '/admin/">' . $config['BASE_URL'] . '/admin/</a></p><br />';
					$message .= '<p>Thank you,</p>';
					$message .= '<p>' . $config['site_name'] . '</p>';
					$message .= '<p><a href="' . $config['BASE_URL'] . '">' . $config['BASE_URL'] . '</a></p>';
	
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
					$headers .= 'From: <' . $config['noreply_email'] . '>' . "\r\n";
					
					if ( mail($email, $subject, $message, $headers) ) {
						$msg = '<div class="alert alert-success" role="alert">Check your email for new login credentials!</div>';
					} else {
						$msg = '<div class="alert alert-danger" role="alert">Something went wrong, please try again!</div>';
					}				
					//Email code ends here	
				} else {
					$msg = '<div class="alert alert-danger" role="alert">Something went wrong, please try again!</div>';
				}
			} else {
				$msg = '<div class="alert alert-danger" role="alert">Your account is block, please contact with admin!</div>';
			}
        } else {
            $msg = '<div class="alert alert-danger" role="alert">Your email is not register yet!</div>';
        }
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);


$smarty->assign('page_title', $seo['admin_forgot_pass_title']);
$smarty->assign('page_keywords', $seo['admin_forgot_pass_keywords']);
$smarty->assign('page_description', $seo['admin_forgot_pass_desc']);
$smarty->assign('page_keywords', $seo['admin_forgot_pass_author']);

$smarty->display('header.tpl');
$smarty->display('forgot-password.tpl');
$smarty->display('footer.tpl');
?>