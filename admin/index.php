<?php
define('_SMARTY_STARTED', TRUE);
define('_ADMIN_STARTED', TRUE);

require_once dirname(dirname(__FILE__)) . '/config/config.php';

if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
	SMRedirect::go($config['BASE_URL'] . '/admin/dashboard/');
}

$error = NULL;
if (isset($_POST['submit_login'])) {
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
	
    if (empty($email) || empty($password)) {
        $error = '<div class="alert alert-danger" role="alert">Please enter email and password!</div>';
    } else {
		$sha1_password = sha1($password);
		$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `email` = '$email' AND `password` = '$sha1_password' LIMIT 0, 1";
		$rs = $conn->execute($query);
		if ( $rs ) {
			$num = $rs->numrows();
			if ( $num == 1 ) {
				$user = $rs->getrows();
				$status = $user[0]['status'];
				if ( $status == 1 ) {
					if (isset($_POST['remember'])) {
						$year = time() + 365*86400;
						setcookie('remember_email', $email, $year);
						setcookie('remember_pass', $password, $year);
					} elseif (!isset($_POST['remember'])) {
						if (isset($_COOKIE['remember_email']) && isset($_COOKIE['remember_pass']) ) {
							$past = time() - 100;
							setcookie('remember_email', '', $past);
							setcookie('remember_pass', '', $past);
						}
					}
					$_SESSION['AUID'] = $user[0]['UID'];
					$_SESSION['AUNAME'] = $user[0]['name'];
					$_SESSION['AUEMAIL'] = $user[0]['email'];
					$_SESSION['AUPSWD'] = $user[0]['password'];
					$_SESSION['AUROLE'] = $user[0]['role'];
					SMRedirect::go($config['BASE_URL'] . '/admin/dashboard/');
				} else {
					$error = '<div class="alert alert-danger" role="alert">Your account is block!</div>';
				}
			} else {
				$error = '<div class="alert alert-danger" role="alert">Invalid username or password!</div>';
			}
		} else {
			$error = '<div class="alert alert-danger" role="alert">Invalid username or password!</div>';
		}
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

if ( isset($_COOKIE['remember_email']) && isset($_COOKIE['remember_pass']) ) {
	$remember_email = $_COOKIE['remember_email'];
	$remember_pass = $_COOKIE['remember_pass'];
} else {
	$remember_email = '';
	$remember_pass = '';
}


$smarty->assign('loggedin', $login);
$smarty->assign('error', $error);

$smarty->assign('remember_email', $remember_email);
$smarty->assign('remember_pass', $remember_pass);

$smarty->assign('page_title', $seo['admin_login_title']);
$smarty->assign('page_keywords', $seo['admin_login_keywords']);
$smarty->assign('page_description', $seo['admin_login_desc']);
$smarty->assign('page_keywords', $seo['admin_login_author']);

$smarty->display('header.tpl');
$smarty->display('index.tpl');
$smarty->display('footer.tpl');
?>