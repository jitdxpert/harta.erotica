<?php
define('_SMARTY_STARTED', TRUE);
define('_ADMIN_STARTED', TRUE);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_SESSION['AUROLE']) || $_SESSION['AUROLE'] != 'admin' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/accounts/');
}

if (isset($_GET['UID']) && isset($_GET['action']) && $_GET['action'] == 'delete') {
    $UID = trim($_GET['UID']);
    $query = "DELETE FROM `" . $config['db_prefix'] . "users` WHERE `UID` = $UID";
    $rs = $conn->execute($query);
} else {
	$rs = false;
}
if ($rs) {
	SMRedirect::go($config['BASE_URL'] . '/admin/accounts.php?message=3&deleted');
} else {
	SMRedirect::go($config['BASE_URL'] . '/admin/accounts.php?message=0&unknown');
}