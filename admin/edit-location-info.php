<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_GET['LID']) || !isset($_GET['action']) || $_GET['action'] != 'edit' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations/');
}

if ( $_SESSION['AUROLE'] == 'admin' ) {
	
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
} else {
	
	$UID = $_SESSION['AUID'];
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `loc_user` = $UID ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
}

$query = "SELECT * FROM `" . $config['db_prefix'] . "users` ORDER BY `UID` DESC";
$rs = $conn->execute($query);
$num = $rs->numrows();
if ( $num > 0 ) {
	$users = $rs->getrows();
} else {
	$users = NULL;
}


$loc_type = addslashes($_GET['type']);
$LID = addslashes($_GET['LID']);
$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
$rs = $conn->execute($query);
$location = $rs->getrows();
$location = $location[0];

$queryService 			= "SELECT * FROM `" . $config['db_prefix'] . "services` WHERE `LID` = $LID";
$rsService 				= $conn->execute($queryService);
if ( $rsService ) {
	$dataService 		= $rsService->getrows();
} else {
	$dataService 		= array();
}

if ( isset($_POST['update_profil']) ) {
	// IMAGE LIBRARY
	$image = new SMImageConv();
    $time = time();
	
	// LOCATION NAME
	$loc_name = addslashes($_POST['loc_name']);
	
	// ADDRESS
    $loc_latitude = addslashes($_POST['loc_latitude']);
    $loc_longitude = addslashes($_POST['loc_longitude']);
    $loc_address = addslashes($_POST['loc_address']);
	
	// LOGO
	$old_logo = addslashes($location['loc_logo']);
	$loc_logos = NULL;
	$loc_logo = $_FILES['loc_logo'];
	if (!empty($loc_logo['name'][0])) {
		for ($i = 0; $i < count($loc_logo['name']); $i++) {
			if ($loc_logo['tmp_name'][$i] != '') {
				if (is_uploaded_file($loc_logo['tmp_name'][$i]) && getimagesize($loc_logo['tmp_name'][$i])) {
					$ext = strtolower(substr($loc_logo['name'][$i], strrpos($loc_logo['name'][$i], '.') + 1));
					if (!check_image($loc_logo['tmp_name'][$i], $ext)) {
						continue;
					}
					$logo_name = $time . '_' . $i . '.jpg';
					$src = $loc_logo['tmp_name'][$i];
					
					$dst = $config['LOC_LOGO_DIR'] . '/org-' . $logo_name;
					$image->process($src, $dst, 'SAME', 0, 0);
					$image->resize(true, true);
					
					$dst = $config['LOC_LOGO_DIR'] . '/list-' . $logo_name;
					$image->process($src, $dst, 'EXACT', 299, 181);
					$image->resize(true, true);
					
					$dst = $config['LOC_LOGO_DIR'] . '/' . $logo_name;
					$image->process($src, $dst, 'EXACT', 200, 100);
					$image->resize(true, true);
					
					$loc_logos .= $logo_name . '|';
				}
			}
		}
		$loc_logos = trim(substr($loc_logos, 0, -1));
		
		@unlink($config['LOC_LOGO_DIR'] . '/org-' . $old_logo);
		@unlink($config['LOC_LOGO_DIR'] . '/' . $old_logo);
		
	} else {
		$loc_logos = $old_logo;
	}
	
	// BANNER
	$old_banner = addslashes($location['loc_banners']);
	$loc_banners = NULL;
	$loc_banner = $_FILES['loc_banner'];
	if (!empty($loc_banner['name'][0])) {
		for ($i = 0; $i < count($loc_banner['name']); $i++) {
			if ($loc_banner['tmp_name'][$i] != '') {
				if (is_uploaded_file($loc_banner['tmp_name'][$i]) && getimagesize($loc_banner['tmp_name'][$i])) {
					$ext = strtolower(substr($loc_banner['name'][$i], strrpos($loc_banner['name'][$i], '.') + 1));
					if (!check_image($loc_banner['tmp_name'][$i], $ext)) {
						continue;
					}
					$banner_name = $time . '_' . $i . '.jpg';
					$src = $loc_banner['tmp_name'][$i];
					
					$dst = $config['LOC_BANNER_DIR'] . '/org-' . $banner_name;
					$image->process($src, $dst, 'SAME', 0, 0);
					$image->resize(true, true);
					
					$dst = $config['LOC_BANNER_DIR'] . '/' . $banner_name;
					$image->process($src, $dst, 'EXACT', 840, 140);
					$image->resize(true, true);
					
					$loc_banners .= $banner_name . '|';
				}
			}
		}
		$loc_banners = trim(substr($loc_banners, 0, -1));
		
		@unlink($config['LOC_BANNER_DIR'] . '/org-' . $old_banner);
		@unlink($config['LOC_BANNER_DIR'] . '/' . $old_banner);
		
	} else {
		$loc_banners = $old_banner;
	}
	
	// LOC DESC
	$loc_desc = addslashes($_POST['loc_desc']);
	
	// PAYMENT
    $loc_payment = $_POST['loc_payment'];
	$loc_payments = '';
	for ($i = 0; $i < count($loc_payment); $i++) {
		$loc_payments .= $loc_payment[$i] . ', ';
	}
	$loc_payments = trim(substr($loc_payments, 0, -2));
	
	// FILTERS
    $loc_filter = $_POST['loc_filter'];
	$loc_filters = '';
	for ($i = 0; $i < count($loc_filter); $i++) {
		$loc_filters .= $loc_filter[$i] . ', ';
	}
	$loc_filters = trim(substr($loc_filters, 0, -2));
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_name` = '$loc_name', `loc_latitude` = '$loc_latitude', `loc_longitude` = '$loc_longitude', `loc_address` = '$loc_address', `loc_logo` = '$loc_logos', `loc_banners` = '$loc_banners', `loc_desc` = '$loc_desc', `loc_payments` = '$loc_payments', `loc_filters` = '$loc_filters' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=1&profil-created#servicii');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&profil-error#profil');
	}
}

if ( isset($_POST['update_servicii']) ) {
	// SERVICE & PRICING
	$loc_free = addslashes($_POST['loc_free']);
	$loc_service_desc = addslashes($_POST['loc_service_desc']);
	$SIDs = $_POST['loc_SID'];
	$services = $_POST['loc_services'];
	$durations = $_POST['loc_durations'];
	$prices = $_POST['loc_prices'];
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_free` = '$loc_free', `loc_service_desc` = '$loc_service_desc' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( count($services) > count($dataService) ) {
		for ( $i=0; $i < count($dataService); $i++ ) {
			$queryS = "UPDATE `" . $config['db_prefix'] . "services` SET `type` = '$loc_type', `service` = '" . $services[$i] . "', `duration` = '" . $durations[$i] . "' `price` = " . $prices[$i] . " WHERE `SID` = " . $SIDs[$i] . " AND `LID` = $LID";
			$rsS = $conn->execute($queryS);
		} 
		for ( $i=count($dataService); $i < count($services); $i++ ) {
			$queryS = "INSERT INTO `" . $config['db_prefix'] . "services` (`SID`, `LID`, `type`, `service`, `duration`, `price`, `created`) VALUES(NULL, $LID, '$loc_type', '" . $services[$i] . "', '" . $durations[$i] . "', " . $prices[$i] . ", NOW())";
			$rsS = $conn->execute($queryS);
		} 
	}else {
		for ( $i=0; $i < count($services); $i++ ) {
			$queryS = "UPDATE `" . $config['db_prefix'] . "services` SET `type` = '$loc_type', `service` = '" . $services[$i] . "', `duration` = '" . $durations[$i] . "', `price` = " . $prices[$i] . " WHERE `SID` = " . $SIDs[$i] . " AND `LID` = $LID";
			$rsS = $conn->execute($queryS);
		} 
	}
	if ( $rs && $rsS ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=1&servicii-created#program');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&servicii-error#servicii');
	}
}

if ( isset($_POST['update_program']) ) {
	// SCHEDULE
	$loc_open = $_POST['loc_open'];
	$loc_close = $_POST['loc_close'];
	$loc_mon_time = $loc_open[0] . '-' . $loc_close[0];
	$loc_tue_time = $loc_open[1] . '-' . $loc_close[1];
	$loc_wed_time = $loc_open[2] . '-' . $loc_close[2];
	$loc_thu_time = $loc_open[3] . '-' . $loc_close[3];
	$loc_fri_time = $loc_open[4] . '-' . $loc_close[4];
	$loc_sat_time = $loc_open[5] . '-' . $loc_close[5];
	$loc_sun_time = $loc_open[6] . '-' . $loc_close[6];
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_monday` = '$loc_mon_time', `loc_tuesday` = '$loc_tue_time', `loc_wednesday` = '$loc_wed_time', `loc_thursday` = '$loc_thu_time', `loc_friday` = '$loc_fri_time', `loc_saturday` = '$loc_sat_time', `loc_sunday` = '$loc_sun_time' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=1&program-created#galerie-locatie');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&program-error#program');
	}
}

if ( isset($_POST['update_gallery']) ) {
	// IMAGE LIBRARY
	$image = new SMImageConv();
    $time = time();
	
	// LOCATION GALLERY
	$old_gallery = addslashes($_POST['old_gallery']);
	$old_gallery_visibility = addslashes($location['loc_gallery_visibility']);
	$loc_galleries = NULL;
	$loc_gallery = $_FILES['loc_gallery'];
	if (!empty($loc_gallery['name'][0])) {
		for ($i = 0; $i < count($loc_gallery['name']); $i++) {
			if ($loc_gallery['tmp_name'][$i] != '') {
				if (is_uploaded_file($loc_gallery['tmp_name'][$i]) && getimagesize($loc_gallery['tmp_name'][$i])) {
					$ext = strtolower(substr($loc_gallery['name'][$i], strrpos($loc_gallery['name'][$i], '.') + 1));
					if (!check_image($loc_gallery['tmp_name'][$i], $ext)) {
						continue;
					}
					$gallery_name = $time . '_' . $i . '.jpg';
					$src = $loc_gallery['tmp_name'][$i];
					
					$dst = $config['LOC_GALLERY_DIR'] . '/org-' . $gallery_name;
					$image->process($src, $dst, 'SAME', 0, 0);
					$image->resize(true, true);
					
					$dst = $config['LOC_GALLERY_DIR'] . '/thumb-' . $gallery_name;
					$image->process($src, $dst, 'EXACT', 100, 80);
					$image->resize(true, true);
					
					$dst = $config['LOC_GALLERY_DIR'] . '/list-' . $gallery_name;
					$image->process($src, $dst, 'MAX_HEIGHT', 299, 181);
					$image->resize(true, true);
					
					$dst = $config['LOC_GALLERY_DIR'] . '/big-' . $gallery_name;
					$image->process($src, $dst, 'MAX_HEIGHT', 750, 400);
					$image->resize(true, true);
					
					$loc_galleries .= $gallery_name . '|';
					$loc_gallery_visibility .= $gallery_name . '|';
				}
			}
		}
		$loc_galleries = trim(substr($loc_galleries, 0, -1));
		$loc_galleries = $old_gallery . '|' . $loc_galleries;
		$loc_galleries = str_ireplace('||', '|', $loc_galleries);
		$loc_galleries = rtrim($loc_galleries, '|');
		$loc_galleries = ltrim($loc_galleries, '|');
		
		$loc_gallery_visibility = trim(substr($loc_gallery_visibility, 0, -1));
		$loc_gallery_visibility = $old_gallery_visibility . '|' . $loc_gallery_visibility;
		$loc_gallery_visibility = str_ireplace('||', '|', $loc_gallery_visibility);
		$loc_gallery_visibility = rtrim($loc_gallery_visibility, '|');
		$loc_gallery_visibility = ltrim($loc_gallery_visibility, '|');
	} else {
		$loc_galleries = $old_gallery;
		$loc_gallery_visibility = $old_gallery_visibility;
	}
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_galleries` = '$loc_galleries', `loc_gallery_visibility` = '$loc_gallery_visibility' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=1&gallery-created#galerie-modele');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&gallery-error#galerie-locatie');
	}
}
	
if ( isset($_POST['update_model']) ) {
	// IMAGE LIBRARY
	$image = new SMImageConv();
    $time = time();
	
	// LOCATION MODEL
	$old_model = addslashes($_POST['old_model']);
	$old_model_visibility = addslashes($location['loc_model_visibility']);
	$loc_models = NULL;
	$loc_model = $_FILES['loc_model'];
	if (!empty($loc_model['name'][0])) {
		for ($i = 0; $i < count($loc_model['name']); $i++) {
			if ($loc_model['tmp_name'][$i] != '') {
				if (is_uploaded_file($loc_model['tmp_name'][$i]) && getimagesize($loc_model['tmp_name'][$i])) {
					$ext = strtolower(substr($loc_model['name'][$i], strrpos($loc_model['name'][$i], '.') + 1));
					if (!check_image($loc_model['tmp_name'][$i], $ext)) {
						continue;
					}
					$model_name = $time . '_' . $i . '.jpg';
					$src = $loc_model['tmp_name'][$i];
					
					$dst = $config['LOC_MODEL_DIR'] . '/org-' . $model_name;
					$image->process($src, $dst, 'SAME', 0, 0);
					$image->resize(true, true);
					
					$dst = $config['LOC_MODEL_DIR'] . '/thumb-' . $model_name;
					$image->process($src, $dst, 'EXACT', 100, 80);
					$image->resize(true, true);
					
					$dst = $config['LOC_MODEL_DIR'] . '/list-' . $model_name;
					$image->process($src, $dst, 'MAX_HEIGHT', 299, 181);
					$image->resize(true, true);
					
					$dst = $config['LOC_MODEL_DIR'] . '/big-' . $model_name;
					$image->process($src, $dst, 'MAX_HEIGHT', 750, 400);
					$image->resize(true, true);
					
					$loc_models .= $model_name . '|';
					$loc_model_visibility .= $model_name . '|';
				}
			}
		}
		$loc_models = trim(substr($loc_models, 0, -1));
		$loc_models = $old_model . '|' . $loc_models;
		$loc_models = str_ireplace('||', '|', $loc_models);
		$loc_models = rtrim($loc_models, '|');
		$loc_models = ltrim($loc_models, '|');
		
		$loc_model_visibility = trim(substr($loc_model_visibility, 0, -1));
		$loc_model_visibility = $old_model_visibility . '|' . $loc_model_visibility;
		$loc_model_visibility = str_ireplace('||', '|', $loc_model_visibility);
		$loc_model_visibility = rtrim($loc_model_visibility, '|');
		$loc_model_visibility = ltrim($loc_model_visibility, '|');
	} else {
		$loc_models = $old_model;
		$loc_model_visibility = $old_model_visibility;
	}
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_models` = '$loc_models', `loc_model_visibility` = '$loc_model_visibility' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=1&model-created#contact');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&model-error#galerie-modele');
	}
}
	
if ( isset($_POST['update_contact']) ) {
	// CONTACT
	$loc_phone = addslashes($_POST['loc_phone']);
    $loc_website = addslashes($_POST['loc_website']);
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_phone` = '$loc_phone', `loc_website` = '$loc_website' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=1&contact-created#joburi');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&contact-error#contact');
	}
}

if ( isset($_POST['update_joburi']) ) {
	// JOB OPPOTUNITY
    $loc_jobdesc = addslashes($_POST['loc_jobdesc']);
	$loc_jobreq = addslashes($_POST['loc_jobreq']);
	$loc_jobbene = addslashes($_POST['loc_jobbene']);
	$loc_jobresp = addslashes($_POST['loc_jobresp']);
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_jobdesc` = '$loc_jobdesc', `loc_jobreq` = '$loc_jobreq', `loc_jobbene` = '$loc_jobbene', `loc_jobresp` = '$loc_jobresp' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/locations/?message=2&location-updated');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&joburi-error');
	}
}

$query 					= "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
$rs 					= $conn->execute($query);
$location				= $rs->getrows();
$location 				= $location[0];
// PROFIL
$loc_name 				= $location['loc_name'];
$loc_latitude 			= $location['loc_latitude'];
$loc_longitude 			= $location['loc_longitude'];
$loc_address 			= $location['loc_address'];
$loc_logos 				= $location['loc_logo'];
$loc_banners 			= $location['loc_banners'];
$loc_desc 				= $location['loc_desc'];
$loc_payments 			= explode(', ', $location['loc_payments']);
$loc_filters 			= explode(', ', $location['loc_filters']);
// SERVICII
$loc_free 				= $location['loc_free'];
$loc_service_desc 		= $location['loc_service_desc'];
$queryService 			= "SELECT * FROM `" . $config['db_prefix'] . "services` WHERE `LID` = $LID";
$rsService 				= $conn->execute($queryService);
if ( $rsService ) {
	$dataService 		= $rsService->getrows();
} else {
	$dataService 		= array();
}
// PROGRAM
$loc_mon_time 			= explode('-', $location['loc_monday']);
$loc_tue_time			= explode('-', $location['loc_tuesday']);
$loc_wed_time 			= explode('-', $location['loc_wednesday']);
$loc_thu_time 			= explode('-', $location['loc_thursday']);
$loc_fri_time 			= explode('-', $location['loc_friday']);
$loc_sat_time 			= explode('-', $location['loc_saturday']);
$loc_sun_time 			= explode('-', $location['loc_sunday']);
// GALERIE LOCATIE
$loc_galleries 			= explode('|', $location['loc_galleries']);
$loc_gallery_visibility = explode('|', $location['loc_gallery_visibility']);
// GALERIE MODELE
$loc_models 		  	= explode('|', $location['loc_models']);
$loc_model_visibility 	= explode('|', $location['loc_model_visibility']);
// CONTACT
$loc_phone 	 			= $location['loc_phone'];
$loc_website 			= $location['loc_website'];
// JABURI
$loc_jobdesc 			= $location['loc_jobdesc'];
$loc_jobreq 			= $location['loc_jobreq'];
$loc_jobbene 			= $location['loc_jobbene'];
$loc_jobresp 			= $location['loc_jobresp'];


$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
			if ( isset($_GET['profil-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['servicii-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['program-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['gallery-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['model-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['contact-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['joburi-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			}
            break;
        case 1:
			if ( isset($_GET['info-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Location info created!</div>';
			} elseif ( isset($_GET['profil-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Profil info updated!</div>';
			} elseif ( isset($_GET['servicii-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Servicii info updated!</div>';
			} elseif ( isset($_GET['program-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Program info updated!</div>';
			} elseif ( isset($_GET['gallery-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Galerie info updated!</div>';
			} elseif ( isset($_GET['model-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Modele info updated!</div>';
			} elseif ( isset($_GET['contact-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Contact info updated!</div>';
			} elseif ( isset($_GET['joburi-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Joburi info updated!</div>';
			}
            break;
        case 2:
            $msg = '<div class="alert alert-success" role="alert">Location info updated!</div>';
            break;
        case 3:
            $msg = '<div class="alert alert-success" role="alert">Location info deleted!</div>';
            break;
		case 4:
            $msg = '<div class="alert alert-danger" role="alert">All field(s) are required!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);
$smarty->assign('locations', $locations);

$smarty->assign('loc_type', $loc_type);
$smarty->assign('LID', $LID);
$smarty->assign('location', $location);

// PROFIL
$smarty->assign('loc_name', $loc_name);
$smarty->assign('loc_latitude', $loc_latitude);
$smarty->assign('loc_longitude', $loc_longitude);
$smarty->assign('loc_address', $loc_address);
$smarty->assign('loc_logo', $loc_logos);
$smarty->assign('loc_banners', $loc_banners);
$smarty->assign('loc_desc', $loc_desc);
$smarty->assign('loc_payments', $loc_payments);
$smarty->assign('loc_filters', $loc_filters);
// SERVICII
$smarty->assign('loc_free', $loc_free);
$smarty->assign('loc_service_desc', $loc_service_desc);
$smarty->assign('loc_services', $dataService);
// PROGRAM
$smarty->assign('loc_monday_open', $loc_mon_time[0]);
$smarty->assign('loc_monday_close', $loc_mon_time[1]);
$smarty->assign('loc_tuesday_open', $loc_tue_time[0]);
$smarty->assign('loc_tuesday_close', $loc_tue_time[1]);
$smarty->assign('loc_wednesday_open', $loc_wed_time[0]);
$smarty->assign('loc_wednesday_close', $loc_wed_time[1]);
$smarty->assign('loc_thursday_open', $loc_thu_time[0]);
$smarty->assign('loc_thursday_close', $loc_thu_time[1]);
$smarty->assign('loc_friday_open', $loc_fri_time[0]);
$smarty->assign('loc_friday_close', $loc_fri_time[1]);
$smarty->assign('loc_saturday_open', $loc_sat_time[0]);
$smarty->assign('loc_saturday_close', $loc_sat_time[1]);
$smarty->assign('loc_sunday_open', $loc_sun_time[0]);
$smarty->assign('loc_sunday_close', $loc_sun_time[1]);
// GALERIE LOCATIE
$smarty->assign('loc_galleries', $loc_galleries);
$smarty->assign('loc_gallery_visibility', $loc_gallery_visibility);
// GALERIE MODELE
$smarty->assign('loc_models', $loc_models);
$smarty->assign('loc_model_visibility', $loc_model_visibility);
// CONTACT
$smarty->assign('loc_phone', $loc_phone);
$smarty->assign('loc_website', $loc_website);
// JABURI
$smarty->assign('loc_jobdesc', $loc_jobdesc);
$smarty->assign('loc_jobreq', $loc_jobreq);
$smarty->assign('loc_jobbene', $loc_jobbene);
$smarty->assign('loc_jobresp', $loc_jobresp);


$smarty->assign('page_title', $seo['admin_select_location_title']);
$smarty->assign('page_keywords', $seo['admin_select_location_keywords']);
$smarty->assign('page_description', $seo['admin_select_location_desc']);
$smarty->assign('page_author', $seo['admin_select_location_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('edit-location-info.tpl');
$smarty->display('footer.tpl');
?>