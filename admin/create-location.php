<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_SESSION['AUROLE']) || $_SESSION['AUROLE'] != 'admin' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations/');
}

$query = "SELECT * FROM `" . $config['db_prefix'] . "users` ORDER BY `UID` DESC";
$rs = $conn->execute($query);
$num = $rs->numrows();
if ( $num > 0 ) {
	$users = $rs->getrows();
} else {
	$users = NULL;
}

$loc_type = addslashes($_GET['type']);
if ( isset($_POST['save_location']) ) {
	$loc_tip = addslashes($_POST['loc_tip']);
    $loc_user = addslashes($_POST['loc_user']);
	$loc_verify = addslashes($_POST['loc_verify']);
    $loc_status = addslashes($_POST['loc_status']);
	$loc_contact1 = addslashes($_POST['loc_contact1']);
	$loc_contact2 = addslashes($_POST['loc_contact2']);
	$loc_page = $_POST['loc_pages'];
	$loc_pages = '';
	for ($i = 0; $i < count($loc_page); $i++) {
		$loc_pages .= $loc_page[$i] . ', ';
	}
	$loc_pages = trim(substr($loc_pages, 0, -2));
	
	
	$query = "INSERT INTO `" . $config['db_prefix'] . "locations` (`LID`, `loc_type`, `loc_tip`, `loc_user`, `loc_verify`, `loc_status`, `loc_contact1`, `loc_contact2`, `loc_pages`, `created`) VALUES(NULL, '$loc_type', '$loc_tip', $loc_user, '$loc_verify', '$loc_status', '$loc_contact1', '$loc_contact2', '$loc_pages', NOW())";
	$rs = $conn->execute($query);
	if ( $rs ) {
		$queryL = "SELECT MAX(`LID`) AS `LAST_LID` FROM `" . $config['db_prefix'] . "locations`";
		$rsL = $conn->execute($queryL);
		$rows = $rsL->getrows();
		$lastLID = $rows[0]['LAST_LID'];
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $lastLID . '&message=1&info-created');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/create-location/?message=0&unknown');
	}
} else {
	$loc_type 		= NULL;
	$loc_tip 		= NULL;
	$loc_user 		= NULL;
	$loc_verify 	= NULL;
	$loc_status 	= NULL;
	$loc_contact1 	= NULL;
	$loc_contact2 	= NULL;
	$loc_pages		= NULL;
}


$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
            $msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
            break;
        case 1:
            $msg = '<div class="alert alert-success" role="alert">New loction created!</div>';
            break;
        case 2:
            $msg = '<div class="alert alert-success" role="alert">Location info updated!</div>';
            break;
        case 3:
            $msg = '<div class="alert alert-success" role="alert">Location info deleted!</div>';
            break;
		case 4:
            $msg = '<div class="alert alert-danger" role="alert">All field(s) are required!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);
$smarty->assign('users', $users);

$smarty->assign('loc_type', $loc_type);
$smarty->assign('loc_tip', $loc_tip);
$smarty->assign('loc_user', $loc_user);
$smarty->assign('loc_verify', $loc_verify);
$smarty->assign('loc_status', $loc_status);
$smarty->assign('loc_contact1', $loc_contact1);
$smarty->assign('loc_contact2', $loc_contact2);
$smarty->assign('loc_pages', $loc_pages);


$smarty->assign('page_title', $seo['admin_create_location_title']);
$smarty->assign('page_keywords', $seo['admin_create_location_keywords']);
$smarty->assign('page_description', $seo['admin_create_location_desc']);
$smarty->assign('page_author', $seo['admin_create_location_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('create-location.tpl');
$smarty->display('footer.tpl');
?>