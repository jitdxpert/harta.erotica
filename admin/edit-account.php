<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_SESSION['AUROLE']) || $_SESSION['AUROLE'] != 'admin' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/accounts/');
}

if (isset($_GET['UID']) && $_GET['action'] == 'edit') {
	$UID = trim($_GET['UID']);
    $query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `UID` = $UID LIMIT 0, 1";
    $rs = $conn->execute($query);
    $user = $rs->getrows();
    $user = $user[0];
	
	$name = $user['name'];
	$email = $user['email'];
	$info = $user['information'];
	
	if ( isset($_POST['update_account']) ) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$conpass = $_POST['conpass'];
		$info = $_POST['info'];
		
		if ( $name == '' || $email == '' || $info == '' ) {
			SMRedirect::go($config['BASE_URL'] . '/admin/edit-account/?UID=' . $UID . '&action=edit&message=5&required');
		} 
		if ( $password != '' ) {
			if ( $password == $conpass ) {
				$sha1_password = sha1($password);
				$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `email` = '$email' AND `UID` != $UID LIMIT 0, 1";
				$rs = $conn->execute($query);
				$num = $rs->numrows();
				if ( $num == 0 ) {
					$query = "UPDATE `" . $config['db_prefix'] . "users` SET `name` = '$name', `email` = '$email', `password` = '$sha1_password', `information` = '$info' WHERE `UID` = $UID";
				} else {
					SMRedirect::go($config['BASE_URL'] . '/admin/edit-account/?UID=' . $UID . '&action=edit&message=4&duplicate');
				}
			} else {
				SMRedirect::go($config['BASE_URL'] . '/admin/edit-account/?UID=' . $UID . '&action=edit&message=6&password');
			}
		} else {
			$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `email` = '$email' AND `UID` != $UID LIMIT 0, 1";
			$rs = $conn->execute($query);
			$num = $rs->numrows();
			if ( $num == 0 ) {
				$query = "UPDATE `" . $config['db_prefix'] . "users` SET `name` = '$name', `email` = '$email', `information` = '$info' WHERE `UID` = $UID";
			} else {
				SMRedirect::go($config['BASE_URL'] . '/admin/edit-account/?UID=' . $UID . '&action=edit&message=4&duplicate');
			}
		}
		$rs = $conn->execute($query);
		if ( $rs ) {
			SMRedirect::go($config['BASE_URL'] . '/admin/accounts/?message=2&updated');
		} else {
			SMRedirect::go($config['BASE_URL'] . '/admin/edit-account/?UID=' . $UID . '&action=edit&message=0&unknown');
		}
	}
}


$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
            $msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
            break;
        case 1:
            $msg = '<div class="alert alert-success" role="alert">New account created!</div>';
            break;
        case 2:
            $msg = '<div class="alert alert-success" role="alert">Accounts info updated!</div>';
            break;
        case 3:
            $msg = '<div class="alert alert-success" role="alert">Account info deleted!</div>';
            break;
		case 4:
            $msg = '<div class="alert alert-danger" role="alert">Email ID already exists!</div>';
            break;
		case 5:
            $msg = '<div class="alert alert-danger" role="alert">All field(s) are required!</div>';
            break;
		case 6:
            $msg = '<div class="alert alert-danger" role="alert">Confirm Password does not match!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);

$smarty->assign('name', $name);
$smarty->assign('email', $email);
$smarty->assign('info', $info);

$smarty->assign('page_title', $seo['admin_create_account_title']);
$smarty->assign('page_keywords', $seo['admin_create_account_keywords']);
$smarty->assign('page_description', $seo['admin_create_account_desc']);
$smarty->assign('page_author', $seo['admin_create_account_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('edit-account.tpl');
$smarty->display('footer.tpl');
?>