<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_GET['LID']) || !isset($_GET['action']) || $_GET['action'] != 'edit' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations/');
}

if ( $_SESSION['AUROLE'] == 'admin' ) {
	
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
} else {
	
	$UID = $_SESSION['AUID'];
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `loc_user` = $UID ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
}

if ( $_SESSION['AUROLE'] == 'admin' ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "users` ORDER BY `UID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$users = $rs->getrows();
	} else {
		$users = NULL;
	}
	$smarty->assign('users', $users);
}

$LID = addslashes($_GET['LID']);
$loc_type = addslashes($_GET['type']);
if ( isset($_POST['update_location']) ) {
	$loc_tip = addslashes($_POST['loc_tip']);
    $loc_user = addslashes($_POST['loc_user']);
	$loc_verify = addslashes($_POST['loc_verify']);
    $loc_status = addslashes($_POST['loc_status']);
	$loc_contact1 = addslashes($_POST['loc_contact1']);
	$loc_contact2 = addslashes($_POST['loc_contact2']);
	$loc_page = $_POST['loc_pages'];
	$loc_pages = '';
	for ($i = 0; $i < count($loc_page); $i++) {
		$loc_pages .= $loc_page[$i] . ', ';
	}
	$loc_pages = trim(substr($loc_pages, 0, -2));
	
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_type` = '$loc_type', `loc_tip` = '$loc_tip', `loc_user` = $loc_user, `loc_verify` = '$loc_verify', `loc_status` = '$loc_status', `loc_contact1` = '$loc_contact1', `loc_contact2` = '$loc_contact2', `loc_pages` = '$loc_pages' WHERE `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location-info/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=2&update-info');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/edit-location/?type=' . $loc_type . '&LID=' . $LID . '&action=edit&message=0&unknown');
	}
}

$query 			= "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
$rs 			= $conn->execute($query);
$location		= $rs->getrows();
$location 		= $location[0];
$loc_type 		= $location['loc_type'];
$loc_tip 		= $location['loc_tip'];
$loc_user 		= $location['loc_user'];
$loc_verify 	= $location['loc_verify'];
$loc_status 	= $location['loc_status'];
$loc_contact1 	= $location['loc_contact1'];
$loc_contact2 	= $location['loc_contact2'];
$loc_pages		= explode(', ', $location['loc_pages']);


$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
            $msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
            break;
        case 1:
            $msg = '<div class="alert alert-success" role="alert">New loction created!</div>';
            break;
        case 2:
            $msg = '<div class="alert alert-success" role="alert">Location info updated!</div>';
            break;
        case 3:
            $msg = '<div class="alert alert-success" role="alert">Location info deleted!</div>';
            break;
		case 4:
            $msg = '<div class="alert alert-danger" role="alert">All field(s) are required!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);
$smarty->assign('locations', $locations);

$smarty->assign('LID', $LID);
$smarty->assign('loc_type', $loc_type);
$smarty->assign('loc_tip', $loc_tip);
$smarty->assign('loc_user', $loc_user);
$smarty->assign('loc_verify', $loc_verify);
$smarty->assign('loc_status', $loc_status);
$smarty->assign('loc_contact1', $loc_contact1);
$smarty->assign('loc_contact2', $loc_contact2);
$smarty->assign('loc_pages', $loc_pages);


$smarty->assign('page_title', $seo['admin_create_location_title']);
$smarty->assign('page_keywords', $seo['admin_create_location_keywords']);
$smarty->assign('page_description', $seo['admin_create_location_desc']);
$smarty->assign('page_author', $seo['admin_create_location_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('edit-location.tpl');
$smarty->display('footer.tpl');
?>