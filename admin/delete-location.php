<?php
define('_SMARTY_STARTED', TRUE);
define('_ADMIN_STARTED', TRUE);

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_SESSION['AUROLE']) || $_SESSION['AUROLE'] != 'admin' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations/');
}

if (isset($_GET['LID']) && isset($_GET['action']) && $_GET['action'] == 'delete') {
    $LID = trim($_GET['LID']);
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
    $rs = $conn->execute($query);
    $location = $rs->getrows();
    $location = $location[0];
	$banner = $location['loc_banners'];
	$logo = $location['loc_logo'];
	$galleries = explode("|", $location['loc_galleries']);
	$models = explode("|", $location['loc_models']);
	
    $query = "DELETE FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID";
    $rs = $conn->execute($query);
	if ( $rs ) {
		@unlink($config['LOC_BANNER_DIR'] . '/org-' . $banner);
		@unlink($config['LOC_BANNER_DIR'] . '/' . $banner);
		@unlink($config['LOC_LOGO_DIR'] . '/org-' . $logo);
		@unlink($config['LOC_LOGO_DIR'] . '/' . $logo);
		foreach($galleries as $gallery) {
			if ( strpos($gallery, 'hidden-') === false ) {
				@unlink($config['LOC_GALLERY_DIR'] . '/org-' . $gallery);
				@unlink($config['LOC_GALLERY_DIR'] . '/thumb-' . $gallery);
				@unlink($config['LOC_GALLERY_DIR'] . '/list-' . $gallery);
				@unlink($config['LOC_GALLERY_DIR'] . '/big-' . $gallery);
			} else {
				$pic = str_ireplace('hidden-', '', $gallery);
				@unlink($config['LOC_GALLERY_DIR'] . '/org-' . $pic);
				@unlink($config['LOC_GALLERY_DIR'] . '/thumb-' . $pic);
				@unlink($config['LOC_GALLERY_DIR'] . '/list-' . $pic);
				@unlink($config['LOC_GALLERY_DIR'] . '/big-' . $pic);
			}
		}
		foreach($models as $model) {
			if ( strpos($model, 'hidden-') === false ) {
				@unlink($config['LOC_MODEL_DIR'] . '/org-' . $model);
				@unlink($config['LOC_MODEL_DIR'] . '/thumb-' . $model);
				@unlink($config['LOC_MODEL_DIR'] . '/list-' . $model);
				@unlink($config['LOC_MODEL_DIR'] . '/big-' . $model);
			} else {
				$pic = str_ireplace('hidden-', '', $model);
				@unlink($config['LOC_MODEL_DIR'] . '/org-' . $pic);
				@unlink($config['LOC_MODEL_DIR'] . '/thumb-' . $pic);
				@unlink($config['LOC_MODEL_DIR'] . '/list-' . $pic);
				@unlink($config['LOC_MODEL_DIR'] . '/big-' . $pic);
			}
		}
	}
} else {
	$rs = false;
}

if ($rs) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations.php?message=3&deleted');
} else {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations.php?message=0&unknown');
}