<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( $_SESSION['AUROLE'] == 'admin' ) {
	
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
} else {
	
	$UID = $_SESSION['AUID'];
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `loc_user` = $UID ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('locations', $locations);

$smarty->assign('page_title', $seo['admin_dashboard_title']);
$smarty->assign('page_keywords', $seo['admin_dashboard_keywords']);
$smarty->assign('page_description', $seo['admin_dashboard_desc']);
$smarty->assign('page_author', $seo['admin_dashboard_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('dashboard.tpl');
$smarty->display('footer.tpl');
?>