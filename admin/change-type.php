<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_GET['LID']) || !isset($_GET['action']) || !isset($_GET['type']) ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations/');
}

if ( $_SESSION['AUROLE'] == 'admin' ) {
	
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
} else {
	
	$UID = $_SESSION['AUID'];
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `loc_user` = $UID ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	
}

$LID = addslashes($_GET['LID']);
$action = addslashes($_GET['action']);
$type = addslashes($_GET['type']);

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('LID', $LID);
$smarty->assign('action', $action);
$smarty->assign('type', $type);
$smarty->assign('locations', $locations);

$smarty->assign('page_title', $seo['admin_select_location_title']);
$smarty->assign('page_keywords', $seo['admin_select_location_keywords']);
$smarty->assign('page_description', $seo['admin_select_location_desc']);
$smarty->assign('page_author', $seo['admin_select_location_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('change-type.tpl');
$smarty->display('footer.tpl');
?>