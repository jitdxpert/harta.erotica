<?php

define('_SMARTY_STARTED', TRUE);
define('_ADMIN_STARTED', TRUE);

require_once dirname(dirname(__FILE__)) . '/config/config.php';

$_SESSION['AUID']       = '';
$_SESSION['AUEMAIL'] 	= '';
$_SESSION['AUPSWD']     = '';
$_SESSION['AUROLE'] 	= '';
unset($_SESSION['AUID']);
unset($_SESSION['AUEMAIL']);
unset($_SESSION['AUPSWD']);
unset($_SESSION['AUROLE']);	

session_write_close();
header('Location:' . $config['BASE_URL'] . '/admin/');
die();
?>