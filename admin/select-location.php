<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_SESSION['AUROLE']) || $_SESSION['AUROLE'] != 'admin' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations/');
}

$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
            $msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
            break;
        case 1:
            $msg = '<div class="alert alert-success" role="alert">New loction created!</div>';
            break;
        case 2:
            $msg = '<div class="alert alert-success" role="alert">Location info updated!</div>';
            break;
        case 3:
            $msg = '<div class="alert alert-success" role="alert">Location info deleted!</div>';
            break;
		case 4:
            $msg = '<div class="alert alert-danger" role="alert">All field(s) are required!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);

$smarty->assign('page_title', $seo['admin_select_location_title']);
$smarty->assign('page_keywords', $seo['admin_select_location_keywords']);
$smarty->assign('page_description', $seo['admin_select_location_desc']);
$smarty->assign('page_author', $seo['admin_select_location_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('select-location.tpl');
$smarty->display('footer.tpl');
?>