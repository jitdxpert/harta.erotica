<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_SESSION['AUROLE']) || $_SESSION['AUROLE'] != 'admin' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/accounts/');
}

if ( isset($_POST['submit_account']) ) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$conpass = $_POST['conpass'];
	$info = $_POST['info'];
	
	if ( $name != '' && $email != '' && $password != '' && $conpass != '' && $info != '' ) {
		if ( $password == $conpass ) {
			$sha1_password = sha1($password);
			$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `email` = '$email' LIMIT 0, 1";
			$rs = $conn->execute($query);
			$num = $rs->numrows();
			if ( $num == 0 ) {
				$query = "INSERT INTO `" . $config['db_prefix'] . "users` (`UID`, `name`, `email`, `password`, `information`, `created`) VALUES (NULL, '$name', '$email', '$sha1_password', '$info', NOW())";
				$rs = $conn->execute($query);
				if ( $rs ) {
					SMRedirect::go($config['BASE_URL'] . '/admin/accounts/?message=1&created');
				} else {
					$_SESSION['name'] = $name;
					$_SESSION['email'] = $email;
					$_SESSION['info'] = $info;
					SMRedirect::go($config['BASE_URL'] . '/admin/create-account/?message=0&unknown');
				}
			} else {
				$_SESSION['name'] = $name;
				unset($_SESSION['email']);
				$_SESSION['info'] = $info;
				SMRedirect::go($config['BASE_URL'] . '/admin/create-account/?message=4&duplicate');
			}
		} else {
			$_SESSION['name'] = $name;
			$_SESSION['email'] = $email;
			$_SESSION['info'] = $info;
			SMRedirect::go($config['BASE_URL'] . '/admin/create-account/?message=6&password');
		}
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/create-account/?message=5&required');
	}
} else {
	$name = isset($_SESSION['name']) ? $_SESSION['name'] : NULL;
	$email = isset($_SESSION['email']) ? $_SESSION['email'] : NULL;
	$role = isset($_SESSION['info']) ? $_SESSION['info'] : NULL;
}


$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
            $msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
            break;
        case 1:
            $msg = '<div class="alert alert-success" role="alert">New account created!</div>';
            break;
        case 2:
            $msg = '<div class="alert alert-success" role="alert">Accounts info updated!</div>';
            break;
        case 3:
            $msg = '<div class="alert alert-success" role="alert">Account info deleted!</div>';
            break;
		case 4:
            $msg = '<div class="alert alert-danger" role="alert">Email ID already exists!</div>';
            break;
		case 5:
            $msg = '<div class="alert alert-danger" role="alert">All field(s) are required!</div>';
            break;
		case 6:
            $msg = '<div class="alert alert-danger" role="alert">Confirm Password does not match!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);

$smarty->assign('name', $name);
$smarty->assign('email', $email);
$smarty->assign('info', $role);

$smarty->assign('page_title', $seo['admin_create_account_title']);
$smarty->assign('page_keywords', $seo['admin_create_account_keywords']);
$smarty->assign('page_description', $seo['admin_create_account_desc']);
$smarty->assign('page_author', $seo['admin_create_account_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('create-account.tpl');
$smarty->display('footer.tpl');
?>