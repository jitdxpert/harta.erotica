<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( $_SESSION['AUROLE'] != 'admin' ) {
	
	$UID = $_SESSION['AUID'];
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `loc_user` = $UID ORDER BY `LID` DESC";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
	} else {
		$locations = NULL;
	}
	$smarty->assign('locations', $locations);
	
}

$query = "SELECT * FROM `" . $config['db_prefix'] . "users` WHERE `UID` = $UID LIMIT 0, 1";
$rs = $conn->execute($query);
$num = $rs->numrows();
if ( $num > 0 ) {
	$me = $rs->getrows();
} else {
	$me = NULL;
}

if ( isset($_POST['contact_submit']) ) {
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	
	//Email sending code
	$subject = $subject . '-' . $config['site_name'];
	
	$message  = '<p>Contact Person: <strong>' . $me[0]['name'] . '</strong>.</p>';
	$message .= '<p>Email ID: <strong>' . $me[0]['email'] . '</strong>.</p>';
	$message .= '<p>&nbsp;</p>';
	$message .= '<p>Subject: <strong>' . $subject . '</strong></p>';
	$message .= '<p>Message: ' . $message . '</p>';
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: <' . $me[0]['email'] . '>' . "\r\n";
	
	if ( mail($config['admin_email'], $subject, $message, $headers) ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/contact/?message=1&sent');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/contact/?message=0&unknown');
	}				
	//Email code ends here	
	
}

$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
            $msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
            break;
        case 1:
            $msg = '<div class="alert alert-success" role="alert">Thank you for contact with us! We will get back to you soon!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);

$smarty->assign('page_title', $seo['admin_contact_title']);
$smarty->assign('page_keywords', $seo['admin_contact_keywords']);
$smarty->assign('page_description', $seo['admin_contact_desc']);
$smarty->assign('page_author', $seo['admin_contact_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('contact.tpl');
$smarty->display('footer.tpl');
?>