<?php
define( '_SMARTY_STARTED', TRUE );
define( '_ADMIN_STARTED', TRUE );

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once $config['BASE_DIR'] . '/classes/image.class.php';
require_once $config['BASE_DIR'] . '/classes/auth.class.php';
$auth = new SMAuth();
$auth->checkAdmin();

if ( !isset($_SESSION['AUROLE']) || $_SESSION['AUROLE'] != 'admin' ) {
	SMRedirect::go($config['BASE_URL'] . '/admin/locations/');
}

$query = "SELECT * FROM `" . $config['db_prefix'] . "users` ORDER BY `UID` DESC";
$rs = $conn->execute($query);
$num = $rs->numrows();
if ( $num > 0 ) {
	$users = $rs->getrows();
} else {
	$users = NULL;
}


$loc_type = addslashes($_GET['type']);
$LID = addslashes($_GET['LID']);
if ( isset($_POST['save_profil']) ) {
	// IMAGE LIBRARY
	$image = new SMImageConv();
    $time = time();
	
	// LOCATION NAME
	$loc_name = addslashes($_POST['loc_name']);
	
	// ADDRESS
    $loc_latitude = addslashes($_POST['loc_latitude']);
    $loc_longitude = addslashes($_POST['loc_longitude']);
    $loc_address = addslashes($_POST['loc_address']);
	
	// LOGO
	$loc_logos = NULL;
    $loc_logo = $_FILES['loc_logo'];
    for ($i = 0; $i < count($loc_logo['name']); $i++) {
        if ($loc_logo['tmp_name'][$i] != '') {
            if (is_uploaded_file($loc_logo['tmp_name'][$i]) && getimagesize($loc_logo['tmp_name'][$i])) {
                $ext = strtolower(substr($loc_logo['name'][$i], strrpos($loc_logo['name'][$i], '.') + 1));
                if (!check_image($loc_logo['tmp_name'][$i], $ext)) {
                    continue;
                }
                $logo_name = $time . '_' . $i . '.jpg';
                $src = $loc_logo['tmp_name'][$i];
				
                $dst = $config['LOC_LOGO_DIR'] . '/org-' . $logo_name;
                $image->process($src, $dst, 'SAME', 0, 0);
                $image->resize(true, true);
				
				$dst = $config['LOC_LOGO_DIR'] . '/list-' . $logo_name;
                $image->process($src, $dst, 'EXACT', 299, 181);
                $image->resize(true, true);
                
				$dst = $config['LOC_LOGO_DIR'] . '/' . $logo_name;
                $image->process($src, $dst, 'EXACT', 200, 100);
                $image->resize(true, true);
                
				$loc_logos .= $logo_name . '|';
            }
        }
    }
    $loc_logos = trim(substr($loc_logos, 0, -1));
	
	// BANNER
    $loc_banners = NULL;
    $loc_banner = $_FILES['loc_banner'];
    for ($i = 0; $i < count($loc_banner['name']); $i++) {
        if ($loc_banner['tmp_name'][$i] != '') {
            if (is_uploaded_file($loc_banner['tmp_name'][$i]) && getimagesize($loc_banner['tmp_name'][$i])) {
                $ext = strtolower(substr($loc_banner['name'][$i], strrpos($loc_banner['name'][$i], '.') + 1));
                if (!check_image($loc_banner['tmp_name'][$i], $ext)) {
                    continue;
                }
                $banner_name = $time . '_' . $i . '.jpg';
                $src = $loc_banner['tmp_name'][$i];
                
				$dst = $config['LOC_BANNER_DIR'] . '/org-' . $banner_name;
                $image->process($src, $dst, 'SAME', 0, 0);
                $image->resize(true, true);
				
                $dst = $config['LOC_BANNER_DIR'] . '/' . $banner_name;
                $image->process($src, $dst, 'EXACT', 840, 140);
                $image->resize(true, true);
                
				$loc_banners .= $banner_name . '|';
            }
        }
    }
    $loc_banners = trim(substr($loc_banners, 0, -1));
	
	// LOC DESC
	$loc_desc = addslashes($_POST['loc_desc']);
	
	// PAYMENT
    $loc_payment = $_POST['loc_payment'];
	$loc_payments = '';
	for ($i = 0; $i < count($loc_payment); $i++) {
		$loc_payments .= $loc_payment[$i] . ', ';
	}
	$loc_payments = trim(substr($loc_payments, 0, -2));
	
	// FILTERS
    $loc_filter = $_POST['loc_filter'];
	$loc_filters = '';
	for ($i = 0; $i < count($loc_filter); $i++) {
		$loc_filters .= $loc_filter[$i] . ', ';
	}
	$loc_filters = trim(substr($loc_filters, 0, -2));
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_name` = '$loc_name', `loc_latitude` = '$loc_latitude', `loc_longitude` = '$loc_longitude', `loc_address` = '$loc_address', `loc_logo` = '$loc_logos', `loc_banners` = '$loc_banners', `loc_desc` = '$loc_desc', `loc_payments` = '$loc_payments', `loc_filters` = '$loc_filters' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=1&profil-created#servicii');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=0&profil-error#profil');
	}
} else {
	$loc_name 		= NULL;
	$loc_latitude 	= NULL;
	$loc_longitude 	= NULL;
	$loc_address 	= NULL;
	$loc_logos 		= NULL;
	$loc_banners 	= NULL;
	$loc_desc 		= NULL;
	$loc_payments 	= NULL;
	$loc_filters 	= NULL;
}

if ( isset($_POST['save_servicii']) ) {
	// SERVICE & PRICING
	$loc_free = addslashes($_POST['loc_free']);
	$loc_service_desc = addslashes($_POST['loc_service_desc']);
	$services = $_POST['loc_services'];
	$durations = $_POST['loc_durations'];
	$prices = $_POST['loc_prices'];
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_free` = '$loc_free', `loc_service_desc` = '$loc_service_desc' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		for ( $i=0; $i < count($services); $i++ ) {
			$queryS = "INSERT INTO `" . $config['db_prefix'] . "services` (`SID`, `LID`, `type`, `service`, `duration`, `price`, `created`) VALUES(NULL, $LID, '$loc_type', '" . $services[$i] . "', '" . $durations[$i] . "', " . $prices[$i] . ", NOW())";
			$rsS = $conn->execute($queryS);
		}
		if ( $rsS ) {
			SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=1&servicii-created#program');
		} else {
			SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=0&servicii-error#servicii');
		}
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=0&servicii-error#servicii');
	}
} else {
	$loc_services 	= NULL;
	$loc_durations 	= NULL;
	$loc_prices		= NULL;
}

if ( isset($_POST['save_program']) ) {
	// SCHEDULE
	$loc_open = $_POST['loc_open'];
	$loc_close = $_POST['loc_close'];
	$loc_mon_time = $loc_open[0] . '-' . $loc_close[0];
	$loc_tue_time = $loc_open[1] . '-' . $loc_close[1];
	$loc_wed_time = $loc_open[2] . '-' . $loc_close[2];
	$loc_thu_time = $loc_open[3] . '-' . $loc_close[3];
	$loc_fri_time = $loc_open[4] . '-' . $loc_close[4];
	$loc_sat_time = $loc_open[5] . '-' . $loc_close[5];
	$loc_sun_time = $loc_open[6] . '-' . $loc_close[6];
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_monday` = '$loc_mon_time', `loc_tuesday` = '$loc_tue_time', `loc_wednesday` = '$loc_wed_time', `loc_thursday` = '$loc_thu_time', `loc_friday` = '$loc_fri_time', `loc_saturday` = '$loc_sat_time', `loc_sunday` = '$loc_sun_time' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=1&program-created#galerie-locatie');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/locations/?type=' . $loc_type . '&LID=' . $LID . '&message=0&program-error#program');
	}
} else {
	$loc_mon_time = NULL;
	$loc_tue_time = NULL;
	$loc_wed_time = NULL;
	$loc_thu_time = NULL;
	$loc_fri_time = NULL;
	$loc_sat_time = NULL;
	$loc_sun_time = NULL;	
}

if ( isset($_POST['save_gallery']) ) {
	// IMAGE LIBRARY
	$image = new SMImageConv();
    $time = time();
	
	// LOCATION GALLERY
	$loc_galleries = NULL;
	$loc_gallery_visibility = NULL;
    $loc_gallery = $_FILES['loc_gallery'];
    for ($i = 0; $i < count($loc_gallery['name']); $i++) {
        if ($loc_gallery['tmp_name'][$i] != '') {
            if (is_uploaded_file($loc_gallery['tmp_name'][$i]) && getimagesize($loc_gallery['tmp_name'][$i])) {
                $ext = strtolower(substr($loc_gallery['name'][$i], strrpos($loc_gallery['name'][$i], '.') + 1));
                if (!check_image($loc_gallery['tmp_name'][$i], $ext)) {
                    continue;
                }
                $gallery_name = $time . '_' . $i . '.jpg';
                $src = $loc_gallery['tmp_name'][$i];
				
                $dst = $config['LOC_GALLERY_DIR'] . '/org-' . $gallery_name;
                $image->process($src, $dst, 'SAME', 0, 0);
                $image->resize(true, true);
                
				$dst = $config['LOC_GALLERY_DIR'] . '/thumb-' . $gallery_name;
                $image->process($src, $dst, 'EXACT', 100, 80);
                $image->resize(true, true);
				
				$dst = $config['LOC_GALLERY_DIR'] . '/list-' . $gallery_name;
                $image->process($src, $dst, 'MAX_HEIGHT', 299, 181);
                $image->resize(true, true);
				
				$dst = $config['LOC_GALLERY_DIR'] . '/big-' . $gallery_name;
                $image->process($src, $dst, 'MAX_HEIGHT', 750, 400);
                $image->resize(true, true);
                
				$loc_galleries .= $gallery_name . '|';
				$loc_gallery_visibility .= $gallery_name . '|';
            }
        }
    }
    $loc_galleries = trim(substr($loc_galleries, 0, -1));
	$loc_gallery_visibility = trim(substr($loc_gallery_visibility, 0, -1));
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_galleries` = '$loc_galleries', `loc_gallery_visibility` = '$loc_gallery_visibility' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=1&gallery-created#galerie-modele');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/locations/?type=' . $loc_type . '&LID=' . $LID . '&message=0&gallery-error#galerie-locatie');
	}
} else {
	$loc_galleries 			= NULL;
	$loc_gallery_visibility = NULL;
}
	
if ( isset($_POST['save_model']) ) {
	// IMAGE LIBRARY
	$image = new SMImageConv();
    $time = time();
	
	// LOCATION MODEL
	$loc_models = NULL;
	$loc_model_visibility = NULL;
    $loc_model = $_FILES['loc_model'];
    for ($i = 0; $i < count($loc_model['name']); $i++) {
        if ($loc_model['tmp_name'][$i] != '') {
            if (is_uploaded_file($loc_model['tmp_name'][$i]) && getimagesize($loc_model['tmp_name'][$i])) {
                $ext = strtolower(substr($loc_model['name'][$i], strrpos($loc_model['name'][$i], '.') + 1));
                if (!check_image($loc_model['tmp_name'][$i], $ext)) {
                    continue;
                }
                $model_name = $time . '_' . $i . '.jpg';
                $src = $loc_model['tmp_name'][$i];
                
				$dst = $config['LOC_MODEL_DIR'] . '/org-' . $model_name;
                $image->process($src, $dst, 'SAME', 0, 0);
                $image->resize(true, true);
				
				$dst = $config['LOC_MODEL_DIR'] . '/thumb-' . $model_name;
                $image->process($src, $dst, 'EXACT', 100, 80);
                $image->resize(true, true);
				
				$dst = $config['LOC_MODEL_DIR'] . '/list-' . $model_name;
                $image->process($src, $dst, 'MAX_HEIGHT', 299, 181);
                $image->resize(true, true);
				
				$dst = $config['LOC_MODEL_DIR'] . '/big-' . $model_name;
                $image->process($src, $dst, 'MAX_HEIGHT', 750, 400);
                $image->resize(true, true);
                
				$loc_models .= $model_name . '|';
				$loc_model_visibility .= $model_name . '|';
            }
        }
    }
    $loc_models = trim(substr($loc_models, 0, -1));
	$loc_model_visibility = trim(substr($loc_model_visibility, 0, -1));
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_models` = '$loc_models', `loc_model_visibility` = '$loc_model_visibility' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=1&model-created#contact');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=0&model-error#galerie-modele');
	}
} else {
	$loc_models 		  = NULL;
	$loc_model_visibility = NULL;
}
	
if ( isset($_POST['save_contact']) ) {
	// CONTACT
	$loc_phone = addslashes($_POST['loc_phone']);
    $loc_website = addslashes($_POST['loc_website']);
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_phone` = '$loc_phone', `loc_website` = '$loc_website' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=1&contact-created#joburi');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=0&contact-error#contact');
	}
} else {
	$loc_phone 	 = NULL;
	$loc_website = NULL;
}

if ( isset($_POST['save_joburi']) ) {
	// JOB OPPOTUNITY
    $loc_jobdesc = addslashes($_POST['loc_jobdesc']);
	$loc_jobreq = addslashes($_POST['loc_jobreq']);
	$loc_jobbene = addslashes($_POST['loc_jobbene']);
	$loc_jobresp = addslashes($_POST['loc_jobresp']);
	
	$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_jobdesc` = '$loc_jobdesc', `loc_jobreq` = '$loc_jobreq', `loc_jobbene` = '$loc_jobbene', `loc_jobresp` = '$loc_jobresp' WHERE `loc_type` = '$loc_type' AND `LID` = $LID";
	$rs = $conn->execute($query);
	if ( $rs ) {
		SMRedirect::go($config['BASE_URL'] . '/admin/locations/?message=1&joburi-created');
	} else {
		SMRedirect::go($config['BASE_URL'] . '/admin/location-info/?type=' . $loc_type . '&LID=' . $LID . '&message=0&joburi-error');
	}
} else {
	$loc_jobdesc = NULL;
	$loc_jobreq = NULL;
	$loc_jobbene = NULL;
	$loc_jobresp = NULL;
}

$msg = NULL;
if ( isset($_GET['message'])){
    $msg_code = $_GET['message'];
    switch ($msg_code){
        case 0:
			if ( isset($_GET['profil-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['servicii-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['program-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['gallery-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['model-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['contact-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			} elseif ( isset($_GET['joburi-error']) ) {
				$msg = '<div class="alert alert-danger" role="alert">Something wrong, try again!</div>';
			}
            break;
        case 1:
			if ( isset($_GET['info-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Location info created!</div>';
			} elseif ( isset($_GET['profil-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Profil info updated!</div>';
			} elseif ( isset($_GET['servicii-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Servicii info updated!</div>';
			} elseif ( isset($_GET['program-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Program info updated!</div>';
			} elseif ( isset($_GET['gallery-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Galerie info updated!</div>';
			} elseif ( isset($_GET['model-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Modele info updated!</div>';
			} elseif ( isset($_GET['contact-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Contact info updated!</div>';
			} elseif ( isset($_GET['joburi-created']) ) {
				$msg = '<div class="alert alert-success" role="alert">Joburi info updated!</div>';
			}
            break;
        case 2:
            $msg = '<div class="alert alert-success" role="alert">Location info updated!</div>';
            break;
        case 3:
            $msg = '<div class="alert alert-success" role="alert">Location info deleted!</div>';
            break;
		case 4:
            $msg = '<div class="alert alert-danger" role="alert">All field(s) are required!</div>';
            break;
    }
}

$smarty->assign('AUID', '');
$smarty->assign('AUNAME', '');
$smarty->assign('AUEMAIL', '');
$smarty->assign('AUPSWD', '');
$smarty->assign('AUROLE', '');
$login = false;
if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('msg', $msg);

$smarty->assign('loc_name', $loc_name);
$smarty->assign('loc_type', $loc_type);
$smarty->assign('loc_banners', $loc_banners);
$smarty->assign('loc_logo', $loc_logos);
$smarty->assign('loc_galleries', $loc_galleries);
$smarty->assign('loc_models', $loc_models);
$smarty->assign('loc_phone', $loc_phone);
$smarty->assign('loc_website', $loc_website);
$smarty->assign('loc_payment', $loc_payments);
$smarty->assign('loc_latitude', $loc_latitude);
$smarty->assign('loc_longitude', $loc_longitude);
$smarty->assign('loc_address', $loc_address);
$smarty->assign('loc_filter', $loc_filters);
$smarty->assign('loc_services', $loc_services);
$smarty->assign('loc_durations', $loc_durations);
$smarty->assign('loc_jobdesc', $loc_jobdesc);
$smarty->assign('loc_jobreq', $loc_jobreq);
$smarty->assign('loc_jobbene', $loc_jobbene);
$smarty->assign('loc_jobresp', $loc_jobresp);

$smarty->assign('page_title', $seo['admin_select_location_title']);
$smarty->assign('page_keywords', $seo['admin_select_location_keywords']);
$smarty->assign('page_description', $seo['admin_select_location_desc']);
$smarty->assign('page_author', $seo['admin_select_location_author']);

$smarty->display('header.tpl');
$smarty->display('sidebar.tpl');
$smarty->display('location-info.tpl');
$smarty->display('footer.tpl');
?>