<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

class SMAuth {

    public function check() {
        if (isset($_SESSION['UID']) && isset($_SESSION['UEMAIL'])) {
            if ($_SESSION['UID'] != '' && $_SESSION['UEMAIL'] != '') {
                return true;
            }
        }

        global $config;
        $_SESSION['redirect'] = ( isset($_SERVER['REQUEST_URI']) ) ? $_SERVER['REQUEST_URI'] : $config['BASE_URL'];
        SMRedirect::go($config['BASE_URL']);
    }

    public function checkAdmin() {
        global $config;

        $access = false;
        if (isset($_SESSION['AUID']) && isset($_SESSION['AUNAME']) && isset($_SESSION['AUEMAIL']) && isset($_SESSION['AUPSWD'])) {
			if ($_SESSION['AUID'] != '' && $_SESSION['AUNAME'] != '' && $_SESSION['AUEMAIL'] != '' && $_SESSION['AUPSWD'] != '') {
	            $access = true;
			}
        }

        if (!$access) {
            SMRedirect::go($config['BASE_URL'] . '/admin/');
        }
    }

    public function confirm() {
        global $config;

        if ($config['email_verification'] == '0') {
            return true;
        }

        if (isset($_SESSION['UID']) && isset($_SESSION['UEMAIL'])) {
            if (isset($_SESSION['EmailVerified']) && $_SESSION['EmailVerified'] == 'yes') {
                return true;
            }
        }

        $_SESSION['redirect'] = ( isset($_SERVER['REQUEST_URI']) ) ? $_SERVER['REQUEST_URI'] : $config['BASE_URL'];
        SMRedirect::go($config['BASE_URL'] . '/confirm');
    }

}

?>