<div id="wrapper">
	<!-- Left Sidebar -->
    <div id="sidebar-wrapper">
    	<!-- LOGO -->
        <div class="row">
        	<a href="{$base_url}"><img class="logo" src="{$asset_url}/images/logo.png" alt="Harta Erotica" /></a>
        </div>
        <!-- end of LOGO -->
        
        <div role="tabpanel" class="m-top-10">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs text-center" role="tablist">
				<li role="presentation" class="col-lg-4 tab_salon active">
					<a href="#salon" aria-controls="salon" role="tab" data-toggle="tab">SALON MASAJ</a>
				</li>
				<li role="presentation" class="col-lg-4 tab_shop">
					<a href="#shop" aria-controls="shop" role="tab" data-toggle="tab">SEX SHOP</a>                
				</li>
				<li role="presentation" class="col-lg-4 tab_club">
					<a href="#club" aria-controls="club" role="tab" data-toggle="tab">NIGHT CLUB</a>
				</li>
			</ul>
            
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane tab_salon active" id="salon">
                	<!-- Search Box -->
                    <div class="search-box">
                    	<form id="salon_form" method="post" role="form">
                            <!-- LOCATION NAME-->
                            <input type="hidden" name="type" value="salon" />
                            <input type="hidden" name="user_lat" class="user_lat" />
							<input type="hidden" name="user_lng" class="user_lng" />
                            <div class="col-md-4 location">
                                <h4>LOCATIE</h4>
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" placeholder="cauta o locatie" />
                                </div>
                            </div>
                            <!-- PRICE RANGE-->
                            <div class="col-md-4 price">
                                <h4>PRET</h4>
                                <h2>
                                    <span class="leftLabel">0</span> - <span class="rightLabel">500</span> RON
                                </h2>
                                <div class="bar">
                                    <div id="salon_price" class="nstSlider" data-range_min="0" data-range_max="500" data-cur_min="0" data-cur_max="500">
                                        <div class="highlightPanel"></div>
                                        <div class="innerBar"></div>
                                        <div class="leftGrip"></div>
                                        <div class="rightGrip"></div>
                                    </div>
                                    <input type="hidden" name="price" value="0-500" />
                                </div>
                            </div>
                            <!-- FACILITIES-->
                            <div class="col-md-4 facility">
                                <h4>FACILITATI</h4>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="facility[]" value="jacuzzi" />
                                        <span>Jacuzzi</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="lounge-bar" />
                                        <span>Lounge bar</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                	<label>
                                        <input type="checkbox" name="facility[]" value="dusuri" />
                                        <span>Dusuri</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="free-wifi" />
                                        <span>Free Wi-fi</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="facility[]" value="masaj-la-hotel-domiciliu" />
                                        <span>Masaj la hotel / domiciliu</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="petreceri-private" />
                                        <span>Petreceri private</span>
                                    </label>
                                </div>
                            </div>
                        </form>
                        <!-- RESULTS-->
                        <div class="col-md-12 result" id="salon_result"></div>
                    </div>	
                    <!-- end of Search Box -->
                    <div class="clearfix"></div>
                    
					<div id="salon_nano" class="nano">
						<div class="nano-content">
                        	<div class="m-top-10 col-lg-12 text-center">
                            	<img src="{$asset_url}/images/loading.gif" alt="loading" />
                            </div>
                        </div>
					</div>
                </div>
                
                <div role="tabpanel" class="tab-pane tab_shop" id="shop">
                	<!-- Search Box -->
                    <div class="search-box">
                    	<form id="shop_form" method="post" role="form">
                            <!-- LOCATION NAME-->
                            <input type="hidden" name="type" value="shop" />
                            <input type="hidden" name="user_lat" class="user_lat" />
							<input type="hidden" name="user_lng" class="user_lng" />
                            <div class="col-md-4 location">
                                <h4>LOCATIE</h4>
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" placeholder="cauta o locatie" />
                                </div>
                            </div>
                            <!-- PRICE RANGE-->
                            <div class="col-md-4 price">
                                <h4>PRET</h4>
                                <h2>
                                    <span class="leftLabel">0</span> - <span class="rightLabel">500</span> RON
                                </h2>
                                <div class="bar">
                                    <div id="shop_price" class="nstSlider" data-range_min="0" data-range_max="500" data-cur_min="0" data-cur_max="500">
                                        <div class="highlightPanel"></div>
                                        <div class="innerBar"></div>
                                        <div class="leftGrip"></div>
                                        <div class="rightGrip"></div>
                                    </div>
                                    <input type="hidden" name="price" value="0-500" />
                                </div>
                            </div>
                            <!-- FACILITIES-->
                            <div class="col-md-4 facility">
                                <h4>FACILITATI</h4>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="facility[]" value="jacuzzi" />
                                        <span>Jacuzzi</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="lounge-bar" />
                                        <span>Lounge bar</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                	<label>
                                        <input type="checkbox" name="facility[]" value="dusuri" />
                                        <span>Dusuri</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="free-wifi" />
                                        <span>Free Wi-fi</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="facility[]" value="masaj-la-hotel-domiciliu" />
                                        <span>Masaj la hotel / domiciliu</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="petreceri-private" />
                                        <span>Petreceri private</span>
                                    </label>
                                </div>
                            </div>
                        </form>
                        <!-- RESULTS-->
                        <div class="col-md-12 result" id="shop_result"></div>
                    </div>	
                    <!-- end of Search Box -->
                    <div class="clearfix"></div>
                    
					<div id="shop_nano" class="nano">
						<div class="nano-content">
                        	<div class="m-top-10 col-lg-12 text-center">
                            	<img src="{$asset_url}/images/loading.gif" alt="loading" />
                            </div>
                        </div>
					</div>
                </div>
                
                <div role="tabpanel" class="tab-pane tab_club" id="club">
                	<!-- Search Box -->
                    <div class="search-box">
                    	<form id="club_form" method="post" role="form">
                            <!-- LOCATION NAME-->
                            <input type="hidden" name="type" value="club" />
                            <input type="hidden" name="user_lat" class="user_lat" />
							<input type="hidden" name="user_lng" class="user_lng" />
                            <div class="col-md-4 location">
                                <h4>LOCATIE</h4>
                                <div class="input-group">
                                    <input type="text" name="keyword" class="form-control" placeholder="cauta o locatie" />
                                </div>
                            </div>
                            <!-- PRICE RANGE-->
                            <div class="col-md-4 price">
                                <h4>PRET</h4>
                                <h2>
                                    <span class="leftLabel">0</span> - <span class="rightLabel">500</span> RON
                                </h2>
                                <div class="bar">
                                    <div id="club_price" class="nstSlider" data-range_min="0" data-range_max="500" data-cur_min="0" data-cur_max="500">
                                        <div class="highlightPanel"></div>
                                        <div class="innerBar"></div>
                                        <div class="leftGrip"></div>
                                        <div class="rightGrip"></div>
                                    </div>
                                    <input type="hidden" name="price" value="0-500" />
                                </div>
                            </div>
                            <!-- FACILITIES-->
                            <div class="col-md-4 facility">
                                <h4>FACILITATI</h4>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="facility[]" value="jacuzzi" />
                                        <span>Jacuzzi</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="lounge-bar" />
                                        <span>Lounge bar</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                	<label>
                                        <input type="checkbox" name="facility[]" value="dusuri" />
                                        <span>Dusuri</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="free-wifi" />
                                        <span>Free Wi-fi</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="facility[]" value="masaj-la-hotel-domiciliu" />
                                        <span>Masaj la hotel / domiciliu</span>
                                    </label>
                                    <label>
                                        <input type="checkbox" name="facility[]" value="petreceri-private" />
                                        <span>Petreceri private</span>
                                    </label>
                                </div>
                            </div>
                        </form>
                        <!-- RESULTS-->
                        <div class="col-md-12 result" id="club_result"></div>
                    </div>	
                    <!-- end of Search Box -->
                    <div class="clearfix"></div>
                    
					<div id="club_nano" class="nano">
						<div class="nano-content">
                        	<div class="m-top-10 col-lg-12 text-center">
                            	<img src="{$asset_url}/images/loading.gif" alt="loading" />
                            </div>
                        </div>
					</div>
                </div>
            </div>
		</div>
                
    </div>
    <!-- /#sidebar-wrapper -->
    
    <!-- Right Sidebar -->
    <div id="page-content-wrapper">
    	<div class="login-fb-section">
        	<ul>
            	<li><a href="#"><img src="{$asset_url}/images/login.png" alt="login" /></a></li>
                <li><a href="#"><img src="{$asset_url}/images/terms.png" alt="terms" /></a></li>
                <li><a href="#"><img src="{$asset_url}/images/fb.png" alt="login" /></a></li>
            </ul>
        </div>
    	<div id="gmap"></div>
        <div class="menu-toggle-wrap">
            <a href="#menu-toggle" class="menu-toggle slide-out" id="menu-toggle"></a>
        </div>            
    </div>
	<!-- /#page-content-wrapper -->
</div>
<!-- /#wrapper -->

<div class="modal fade" id="salonModal" tabindex="-1" role="dialog" aria-labelledby="salonModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="shopModal" tabindex="-1" role="dialog" aria-labelledby="shopModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="clubModal" tabindex="-1" role="dialog" aria-labelledby="clubModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body"></div>
        </div>
    </div>
</div>