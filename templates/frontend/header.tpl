<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="keywords" content="{if isset($page_keywords) && !empty($page_keywords)}{$page_keywords|escape:'html'}{/if}" />
<meta name="description" content="{if isset($page_description) && !empty($page_description)}{$page_description|escape:'html'}{/if}" />
<meta name="author" content="{if isset($page_author) && !empty($page_author)}{$page_author|escape:'html'}{/if}" />

<title>{if isset($page_title) && $page_title != ''}{$page_title|escape:'html'}{else}{$site_name}{/if}</title>

<link rel="shortcut icon" href="{$asset_url}/images/favicon.ico" />

<!-- Bootstrap Core CSS -->
<link href="{$asset_url}/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/css/jquery.nstSlider.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/css/nanoscroller.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/css/flexslider.css" rel="stylesheet" type="text/css" media="screen" />
<link href="{$asset_url}/css/infowindow.css" rel="stylesheet" type="text/css" />
<!-- Custom CSS -->
<link href="{$asset_url}/css/template-he.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet" type="text/css" />

<script src="{$asset_url}/scripts/modernizr.js" type="text/javascript"></script>
<script src="{$asset_url}/scripts/jquery.min.js" type="text/javascript"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>