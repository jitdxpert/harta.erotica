	<div class="modal-loading hide"></div>
    <!-- Core Scripts - Include with every page -->
	<script type="text/javascript">
	var asset_url = "{$asset_url}";
	var base_url = "{$base_url}";
	</script>
	<script src="{$asset_url}/scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="{$asset_url}/scripts/pace.min.js" type="text/javascript"></script>
    {literal}<script>Pace.options = { ajax: { trackWebSockets: false } }</script>{/literal}
	
    <script src="{$asset_url}/scripts/jquery.easing.js" type="text/javascript"></script>
    <script src="{$asset_url}/scripts/jquery.mousewheel.js" type="text/javascript"></script>
    
	<script src="{$asset_url}/scripts/jquery.nstSlider.min.js" type="text/javascript"></script>
	<script src="{$asset_url}/scripts/jquery.nanoscroller.min.js" type="text/javascript"></script>
    
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&v=3.exp&key=AIzaSyCrQRRmzaIpk2jITNpwVSyUg37mvrQfjzs" type="text/javascript"></script>
	<script src="{$asset_url}/scripts/markerclusterer.js" type="text/javascript"></script>
	<script src="{$asset_url}/scripts/infobox.js"></script>
	
    <!-- Flex slider api -->
    <script defer src="{$asset_url}/scripts/jquery.flexslider-min.js" type="text/javascript"></script>
	<script src="{$asset_url}/scripts/shCore.js" type="text/javascript"></script>
    <script src="{$asset_url}/scripts/shBrushXml.js" type="text/javascript"></script>
    <script src="{$asset_url}/scripts/shBrushJScript.js" type="text/javascript"></script>
    
	<script src="{$asset_url}/scripts/scripts.js"></script>
</body>
</html>