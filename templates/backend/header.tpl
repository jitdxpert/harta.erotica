<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="{if isset($page_keywords) && !empty($page_keywords)}{$page_keywords|escape:'html'}{/if}" />
<meta name="description" content="{if isset($page_description) && !empty($page_description)}{$page_description|escape:'html'}{/if}" />
<meta name="author" content="{if isset($page_author) && !empty($page_author)}{$page_author|escape:'html'}{/if}" />

<title>{if isset($page_title) && $page_title != ''}{$page_title|escape:'html'}{else}{$site_name}{/if}</title>

<link rel="shortcut icon" href="{$asset_url}/images/favicon.ico" />

<link href="{$asset_url}/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
<link href="{$asset_url}/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet" />
<link href="{$asset_url}/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/dist/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/dist/css/hover.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/dist/css/bootstrap-clockpicker.css" rel="stylesheet" type="text/css" />
<link href="{$asset_url}/dist/css/admin.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>