<div id="page-wrapper-loc">
	<nav class="navbar navbar-bordeaux navbar-static-top" role="navigation" style="margin-bottom: 0">
    	<div class="navbar-header">
        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            	<span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-links" href="{$base_url}/admin/edit-location/?type={$loc_type}&LID={$LID}&action=edit">INFORMATII <br>GENERALE</a>
            <a class="navbar-links outline-links links-active" href="">PAGINA DE<br>PREZENTARE</a>
        </div>
    </nav>
    
    <ul class="nav nav-pills" id="LocationTab">
    	<li class="active"><a style="width:115px;" href="#profil" data-toggle="tab" aria-expanded="true">Profil</a></li>
        <li><a href="#servicii" data-toggle="tab" aria-expanded="false">Servicii</a></li>
        <li><a href="#program" data-toggle="tab" aria-expanded="false">Program</a></li>            
        <li><a href="#galerie-locatie" data-toggle="tab" aria-expanded="false">Galerie Locatie</a></li>           
        <li><a href="#galerie-modele" data-toggle="tab" aria-expanded="false">Galerie Modele</a>  </li>
        <li><a href="#contact" data-toggle="tab" aria-expanded="false">Contact</a></li>
        <li><a href="#joburi" data-toggle="tab" aria-expanded="false">Joburi</a></li>
    </ul>
    
    <div class="row" style="padding-top:10px;">
    	<div class="col-lg-12">
        	{if $msg} <div class="col-lg-12">{$msg}</div> {/if}
        	<div class="panel">
            	<div class="panel-body">
                	<div class="row">
                    	<div class="panel-body">
                        	<div class="tab-content">
                            	<div class="tab-pane fade  active in" id="profil">
                                	<form role="form" action="" method="post" enctype="multipart/form-data">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Nume locatie</div>
                                            <div class="form-group col-lg-7">
                                            	<input class="form-control" type="text" name="loc_name" value="{$loc_name}" required />
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                        	<div class="col-lg-3 label">Adresa</div>
                                            <div class="form-group col-lg-7">
                                            	<p><input type="text" placeholder="Latitudine" class="form-control" name="loc_latitude" value="{$loc_latitude}" required /></p>
                                                <p><input type="text" placeholder="Longitudine"  class="form-control" name="loc_longitude" value="{$loc_longitude}" required /></p>
                                                <p><textarea class="form-control" placeholder="Adresa completa" rows="3" name="loc_address" required>{$loc_address}</textarea></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Logo</div>
                                            <div class="form-group col-lg-4">
                                            	<input type="file" name="loc_logo[]" />
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <img src="{$logo_url}/{$loc_logo}" class="thumbnail img-responsive" />
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                        	<div class="col-lg-3 label">Top banner</div>
                                            <div class="form-group col-lg-4">
                                            	<input type="file" name="loc_banner[]" />
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <img src="{$banner_url}/{$loc_banners}" class="thumbnail img-responsive" />
                                            </div>	
                                        </div>
                                        <div class="col-lg-10"> 
                                        	<div class="col-lg-3 label">Descriere completa</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" name="loc_desc" rows="3" required>{$loc_desc}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                        	<div class="col-lg-3 label">Plata</div>
                                            <div class="form-group col-lg-7">
                                            	{assign var=payments value=array('Cash', 'Card', 'Pound', 'Euro', 'Dollar')}
                                                {section name=i loop=$payments}
                                                    <div class="checkbox">
                                                        <label>
                                                            {if $payments[i]|in_array:$loc_payments}
                                                                <input type="checkbox" value="{$payments[i]}" checked="checked" name="loc_payment[]" /> {$payments[i]}
                                                            {else}
                                                                <input type="checkbox" value="{$payments[i]}" name="loc_payment[]" /> {$payments[i]}
                                                            {/if}
                                                        </label>
                                                    </div>
                                                {/section}
                                            </div>
                                        </div>
                                        <div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Filtre</div>
                                            <div class="form-group col-lg-7">
                                            	{assign var=filters value=array('Jacuzzi', 'Lounge bar', 'Dusuri', 'Free Wifi', 'Masaj la hotel / domiciliu', 'Petreceri private')}
                                                {section name=i loop=$filters}
                                                    <div class="checkbox">
                                                        <label>
                                                            {if $filters[i]|in_array:$loc_filters}
                                                                <input type="checkbox" value="{$filters[i]}" checked="checked" name="loc_filter[]" />{$filters[i]}
                                                            {else}
                                                                <input type="checkbox" value="{$filters[i]}" name="loc_filter[]" />{$filters[i]}
                                                            {/if}
                                                        </label>
                                                    </div>
                                                {/section}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="update_profil" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane fade" id="servicii">
                                	<form role="form" action="" method="post">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Gratuitati Servicii:</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_free" required>{$loc_free}</textarea>
                                            </div>
                                        	<div class="col-lg-3 label">Descriere Servicii:</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_service_desc" required>{$loc_service_desc}</textarea>
                                            </div>
                                            {section name=i loop=$loc_services}
                                            	<div class="ServiceRows">
                                                	<input type="hidden" name="loc_SID[]" value="{$loc_services[i].SID}" />
                                                    <div class="col-lg-3 label">Servicii:</div>
                                                    <div class="form-group col-lg-7">
                                                        <input type="text" placeholder="Servicii"  class="form-control" name="loc_services[]" value="{$loc_services[i].service}" />
                                                    </div>
                                                    <div class="col-lg-3 label">Durata:</div>
                                                    <div class="form-group col-lg-7">
                                                        <input type="text" placeholder="Durată"  class="form-control" name="loc_durations[]" value="{$loc_services[i].duration}" />
                                                    </div>
                                                    <div class="col-lg-3 label">Preturi:</div>
                                                    <div class="form-group col-lg-7">
                                                        <input type="number" placeholder="Preturi"  class="form-control" name="loc_prices[]" value="{$loc_services[i].price}" />
                                                    </div>
                                                </div>
                                            {/section}
                                            <div id="MoreServices"></div>
                                            <div class="form-group col-lg-3">
	                                            <a id="add-service" href="javascript:void(0);"><i class="fa fa-plus-circle fa-fw pull-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                            <button type="submit" name="update_servicii" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane fade" id="program">
                                	<form role="form" action="" method="post">
                                    	<div class="col-lg-10">	
                                        	<div class="row">
                                            	<div class="col-lg-2 label">LUNI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" value="{$loc_monday_open}" required />
                                                     	<span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" value="{$loc_monday_close}" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">MARTI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" value="{$loc_tuesday_open}" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" value="{$loc_tuesday_close}" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">		
                                            	<div class="col-lg-2 label">MIERCURI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" value="{$loc_wednesday_open}" required />
                                                       	<span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" value="{$loc_wednesday_close}" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">JOI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" value="{$loc_thursday_open}" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" value="{$loc_thursday_close}" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">VINERI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" value="{$loc_friday_open}" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" value="{$loc_friday_close}" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">SAMBATA</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" value="{$loc_saturday_open}" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" value="{$loc_saturday_close}" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                          	<div class="row">	
                                            	<div class="col-lg-2 label">DUMINICA</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" value="{$loc_sunday_open}" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" value="{$loc_sunday_close}" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="update_program" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                   	</form>  
                               	</div>
                               
                               	<div class="tab-pane fade" id="galerie-locatie">
                               		<form role="form" action="" method="post" enctype="multipart/form-data">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Galerie Imagini</div>
                                            <div class="form-group col-lg-7">
                                            	<input type="file" name="loc_gallery[]" id="gallery" multiple />
                                                <input type="hidden" class="old-files" id="old_gallery" name="old_gallery" value="{$location.loc_galleries}" />
                                            </div>
                                            <div class="form-group col-lg-10">
                                                {if !empty($loc_galleries)}
                                                    <div class="extra-wrap">
                                                        <div class="photos-container" id="gallery-sortable" itemid="{$LID}">
                                                            {section name=i loop=$loc_galleries}
                                                                <div class="photo-wrap" itemid="{$loc_galleries[i]}" {if $loc_gallery_visibility[i]==$loc_galleries[i]} data-id="{$loc_galleries[i]}" {else} data-id="hidden-{$loc_galleries[i]}" {/if}>
                                                                    <img src="{$gallery_url}/thumb-{$loc_galleries[i]}" class="thumbnail" />
                                                                    <div class="delete-cross" itemtype="gallery" id="{$loc_galleries[i]}" data-id="{$loc_gallery_visibility[i]}" title="delete this photo">x</div>
                                                                    {if $loc_gallery_visibility[i]!=$loc_galleries[i]}
                                                                        <div class="visibility-check" itemtype="gallery" style="background-position:100% 0;" itemid="hidden-{$loc_galleries[i]}" title="Visible"></div>
                                                                    {else}
                                                                        <div class="visibility-check" itemtype="gallery" itemid="{$loc_galleries[i]}" title="Hidden"></div>
                                                                    {/if}
                                                                </div>
                                                            {/section}
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="update_gallery" class="btn btn-danger">Upload</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>  
                                </div>
                                
                                <div class="tab-pane fade" id="galerie-modele">
                                	<form role="form" action="" method="post" enctype="multipart/form-data">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Galerie modele</div>
                                            <div class="form-group col-lg-7">
                                            	<input type="file" name="loc_model[]" id="loc_model" multiple />
                                                <input type="hidden" class="old-files" id="old_model" name="old_model" value="{$location.loc_models}" />
                                            </div>
                                            <div class="form-group col-lg-10">
                                                {if !empty($loc_models)}
                                                    <div class="extra-wrap">
                                                        <div class="photos-container" id="model-sortable" itemid="{$LID}">
                                                            {section name=i loop=$loc_models}
                                                                <div class="photo-wrap" itemid="{$loc_models[i]}" {if $loc_model_visibility[i]==$loc_models[i]} data-id="{$loc_models[i]}" {else} data-id="hidden-{$loc_models[i]}" {/if}>
                                                                    <img src="{$model_url}/thumb-{$loc_models[i]}" class="thumbnail" />
                                                                    <div class="delete-cross" itemtype="model" id="{$loc_models[i]}" data-id="{$loc_model_visibility[i]}" title="delete this photo">x</div>
                                                                    {if $loc_model_visibility[i]!=$loc_models[i]}
                                                                        <div class="visibility-check" itemtype="model" style="background-position:100% 0;" itemid="hidden-{$loc_models[i]}" title="Visible"></div>
                                                                    {else}
                                                                        <div class="visibility-check" itemtype="model" itemid="{$loc_models[i]}" title="Hidden"></div>
                                                                    {/if}
                                                                </div>
                                                            {/section}
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="update_model" class="btn btn-danger">Upload</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>  
                                </div>
                                
                                <div class="tab-pane fade" id="contact">
                                	<form role="form" action="" method="post">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Contact</div>
                                            <div class="form-group col-lg-7">
                                            	<p><input type="text" class="form-control" name="loc_phone" placeholder="Phone Number" value="{$loc_phone}" required /></p>
                                                <p><input type="text" class="form-control" name="loc_website" placeholder="Website" value="{$loc_website}" required /></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="update_contact" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>		 
                                    </form>      
                                </div>
                                
                                <div class="tab-pane fade" id="joburi">
                                	<form role="form" action="" method="post">	 
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Descriere</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobdesc" required>{$loc_jobdesc}</textarea>
                                            </div>
                                            <div class="col-lg-3 label">Cerinte</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobreq" required>{$loc_jobreq}</textarea>
                                            </div>
                                            <div class="col-lg-3 label">Beneficii</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobbene" required>{$loc_jobbene}</textarea>
                                            </div>
                                            <div class="col-lg-3 label">Responsabilitati</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobresp" required>{$loc_jobresp}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="update_joburi" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>		 
                                    </form>      
                                </div>
                            </div>
                        </div>
                    	<div class="col-lg-7"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>