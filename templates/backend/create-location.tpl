<div id="page-wrapper-loc">
	<nav class="navbar navbar-bordeaux navbar-static-top" role="navigation" style="margin-bottom:0;">
    	<div class="navbar-header">
        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-links links-active" href="">INFORMATII <br>GENERALE</a>
            <a class="navbar-links outline-links">PAGINA DE<br>PREZENTARE</a>
        </div>
    </nav>
    
    <div class="row" style="padding-top:10px;">
    	<div class="col-lg-12">
        	{if $msg} <div class="col-lg-12">{$msg}</div> {/if}
        	<div class="panel">
            	<div class="panel-body">
                	<div class="row">
                    	<div class="col-lg-7">
                        	<form method="post" action="" role="form">
                            	<div class="col-lg-10">	
                                	<div class="col-lg-3 label">Tip locatie</div>
                                    <div class="form-group col-lg-7">
                                    	<select name="loc_tip" class="form-control" required>
                                        	<option value="Payed">Payed</option>
                                            <option value="Free">Free</option>
                                        </select>
                                    </div>
                                </div>
                               	<div class="col-lg-10">	  
                                	<div class="col-lg-3 label">Cont editare</div>
                                    <div class="form-group col-lg-7">
                                    	<select name="loc_user" class="form-control" required>
                                            {if $users}
                                                {section name=i loop=$users}
                                                    <option value="{$users[i].UID}">{$users[i].email}</option>
                                                {/section}
                                            {/if}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-10">	
                                	<div class="col-lg-3 label">Locatie Verificata</div>
                                    <div class="form-group col-lg-7">
                                    	<select name="loc_verify" class="form-control" required>
                                        	<option value="Y">DA</option>
                                            <option value="N">NU</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                	<div class="col-lg-3 label">Status locatie</div>
                                    <div class="form-group col-lg-7">
                                    	<select name="loc_status" class="form-control" required>
                                        	<option value="show">Publicata</option>
                                            <option value="hide">Inactiva</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-10"> 
                                	<div class="col-lg-3 label">Contact 1</div>
                                    <div class="form-group col-lg-7">
                                    	<textarea name="loc_contact1" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                	<div class="col-lg-3 label">Contact 2</div>
                                  	<div class="form-group col-lg-7">
                                    	<textarea name="loc_contact2" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                              	<div class="col-lg-10">	
                                	<div class="col-lg-3 label">Pagini active</div>
                                  	<div class="form-group col-lg-7">
                                    	<div class="checkbox">
                                        	<label><input name="loc_pages[]" type="checkbox" value="Profil">Profil</label>
                                        </div>
                                        <div class="checkbox">
                                        	<label><input name="loc_pages[]" type="checkbox" value="Servicii">Servicii</label>
                                        </div>
                                        <div class="checkbox">
                                        	<label><input name="loc_pages[]" type="checkbox" value="Galerie locatie">Galerie locatie</label>
                                        </div>
                                        <div class="checkbox">
                                        	<label><input name="loc_pages[]" type="checkbox" value="Galerie Modele">Galerie Modele</label>
                                        </div>
                                        <div class="checkbox">
                                        	<label><input name="loc_pages[]" type="checkbox" value="Contact">Contact</label>
                                        </div>
                                        <div class="checkbox">
                                        	<label><input name="loc_pages[]" type="checkbox" value="Joburi">Joburi</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 pull-right">
                                	<button type="submit" name="save_location" class="btn btn-danger">Salveaza</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>