<div class="panel panel-red">
	<div class="panel-heading title-panel">
    	Contact to Admin
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-7">
            	{if $msg} {$msg} {/if}
                <form action="" method="post" role="form">
                    <div class="form-group col-md-12">
                        <label> Subiect</label>
                        <input class="form-control" name="subject" required />
                    </div>
                    <div class="form-group col-md-12">
                        <label>Message</label>
                        <textarea class="form-control" name="message" required rows="5"></textarea>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" name="contact_submit" class="btn btn-danger">TRIMITE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>