<div class="container">
	<div class="row">
    	<div class="col-md-4 col-md-offset-4">
        	<div class="login-panel panel panel-default">
            	<img style="padding-top:20px;margin:0 auto;display:block;" src="{$asset_url}/images/admin-logo.png" alt="Admin logo" />
                <div class="panel-body" style="padding:50px;">
                	<form action="" method="post" role="form">
                    	{if $msg}<p style="text-align:center;">{$msg}</p>{/if}
                    	<fieldset>
                            <div class="form-group">
                            	<input class="form-control" placeholder="email" name="email" id="email" type="email" required autofocus />
                            </div>
                            <button type="submit" name="submit_login" id="submit_login" class="btn btn-lg btn-danger btn-block">Submit</button><br />
                            <a href="{$base_url}/admin/">Înapoi la autentificare</a>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>