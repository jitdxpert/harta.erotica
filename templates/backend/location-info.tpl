<div id="page-wrapper-loc">
	<nav class="navbar navbar-bordeaux navbar-static-top" role="navigation" style="margin-bottom: 0">
    	<div class="navbar-header">
        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            	<span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-links">INFORMATII <br>GENERALE</a>
            <a class="navbar-links outline-links links-active" href="">PAGINA DE<br>PREZENTARE</a>
        </div>
    </nav>
    
    <ul class="nav nav-pills" id="LocationTab">
    	<li class="active"><a style="width:115px;" href="#profil" data-toggle="tab" aria-expanded="true">Profil</a></li>
        <li><a href="#servicii" data-toggle="tab" aria-expanded="false">Servicii</a></li>
        <li><a href="#program" data-toggle="tab" aria-expanded="false">Program</a></li>            
        <li><a href="#galerie-locatie" data-toggle="tab" aria-expanded="false">Galerie Locatie</a></li>           
        <li><a href="#galerie-modele" data-toggle="tab" aria-expanded="false">Galerie Modele</a>  </li>
        <li><a href="#contact" data-toggle="tab" aria-expanded="false">Contact</a></li>
        <li><a href="#joburi" data-toggle="tab" aria-expanded="false">Joburi</a></li>
    </ul>
    
    <div class="row" style="padding-top:10px;">
    	<div class="col-lg-12">
        	{if $msg} <div class="col-lg-12">{$msg}</div> {/if}
        	<div class="panel">
            	<div class="panel-body">
                	<div class="row">
                    	<div class="panel-body">
                        	<div class="tab-content">
                            	<div class="tab-pane fade  active in" id="profil">
                                	<form role="form" action="" method="post" enctype="multipart/form-data">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Nume locatie</div>
                                            <div class="form-group col-lg-7">
                                            	<input class="form-control" type="text" name="loc_name" required />
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                        	<div class="col-lg-3 label">Adresa</div>
                                            <div class="form-group col-lg-7">
                                            	<p><input type="text" placeholder="Latitudine" class="form-control" name="loc_latitude" required /></p>
                                                <p><input type="text" placeholder="Longitudine"  class="form-control" name="loc_longitude" required /></p>
                                                <p><textarea class="form-control" placeholder="Adresa completa" rows="3" name="loc_address" required></textarea></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Logo</div>
                                            <div class="form-group col-lg-7">
                                            	<input type="file" name="loc_logo[]" required />
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                        	<div class="col-lg-3 label">Top banner</div>
                                            <div class="form-group col-lg-7">
                                            	<input type="file" name="loc_banner[]" required />
                                            </div>
                                        </div>
                                        <div class="col-lg-10"> 
                                        	<div class="col-lg-3 label">Descriere completa</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" name="loc_desc" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                        	<div class="col-lg-3 label">Plata</div>
                                            <div class="form-group col-lg-7">
                                            	<div class="checkbox">
                                                	<label><input type="checkbox" value="Cash" name="loc_payment[]" /> Cash</label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" value="Card" name="loc_payment[]" /> Card</label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" value="Pound" name="loc_payment[]" /> Pound</label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" value="Euro" name="loc_payment[]" /> Euro</label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" value="Dollar" name="loc_payment[]" /> Dollar</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Filtre</div>
                                            <div class="form-group col-lg-7">
                                            	<div class="checkbox">
                                                	<label><input type="checkbox" name="loc_filter[]" value="Jacuzzi" /><span>Jacuzzi</span></label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" name="loc_filter[]" value="Lounge bar" /><span>Lounge bar</span></label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" name="loc_filter[]" value="Dusuri" /><span>Dusuri</span></label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" name="loc_filter[]" value="Free Wifi" /><span>Free Wi-fi</span></label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" name="loc_filter[]" value="Masaj la hotel / domiciliu" /><span>Masaj la hotel / domiciliu</span></label>
                                                </div>
                                                <div class="checkbox">
                                                	<label><input type="checkbox" name="loc_filter[]" value="Petreceri private" /><span>Petreceri private</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="save_profil" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane fade" id="servicii">
                                	<form role="form" action="" method="post">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Gratuitati Servicii:</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_free" required></textarea>
                                            </div>
                                        	<div class="col-lg-3 label">Descriere Servicii:</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_service_desc" required></textarea>
                                            </div>
                                            <div class="ServiceRows">
                                            	<div class="col-lg-3 label">Servicii:</div>
                                                <div class="form-group col-lg-7">
                                                    <input type="text" placeholder="Servicii"  class="form-control" name="loc_services[]" />
                                                </div>
                                                <div class="col-lg-3 label">Durata:</div>
                                                <div class="form-group col-lg-7">
                                                    <input type="text" placeholder="Durată"  class="form-control" name="loc_durations[]" />
                                                </div>
                                                <div class="col-lg-3 label">Preturi:</div>
                                                <div class="form-group col-lg-7">
                                                    <input type="number" placeholder="Preturi"  class="form-control" name="loc_prices[]" />
                                                </div>
                                            </div>
                                            <div id="MoreServices"></div>
                                            <div class="form-group col-lg-3">
	                                            <a id="add-service" href="javascript:void(0);"><i class="fa fa-plus-circle fa-fw pull-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                            <button type="submit" name="save_servicii" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>
                                </div>
                                
                                <div class="tab-pane fade" id="program">
                                	<form role="form" action="" method="post">
                                    	<div class="col-lg-10">	
                                        	<div class="row">
                                            	<div class="col-lg-2 label">LUNI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" required />
                                                     	<span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">MARTI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">		
                                            	<div class="col-lg-2 label">MIERCURI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" required />
                                                       	<span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">JOI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">VINERI</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">	
                                            	<div class="col-lg-2 label">SAMBATA</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                          	<div class="row">	
                                            	<div class="col-lg-2 label">DUMINICA</div>
                                                <div class="form-group col-lg-5">
                                                	<div class="input-group clockpicker">
                                                    	<input type="text" class="form-control timepicker" name="loc_open[]" required />
                                                        <span class="input-group-addon">
                                                        	<span>Deschidere</span>
                                                        </span>
                                                    </div>
                                                    <div class="input-group clockpicker" style="margin-top:10px;">
                                                    	<input type="text" class="form-control timepicker" name="loc_close[]" required />
                                                        <span class="input-group-addon">
                                                        	<span >Inchidere</span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="save_program" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                   	</form>  
                               	</div>
                               
                               	<div class="tab-pane fade" id="galerie-locatie">
                               		<form role="form" action="" method="post" enctype="multipart/form-data">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Galerie Imagini</div>
                                            <div class="form-group col-lg-7">
                                            	<input type="file" name="loc_gallery[]" id="gallery" multiple required />
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="save_gallery" class="btn btn-danger">Upload</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>  
                                </div>
                                
                                <div class="tab-pane fade" id="galerie-modele">
                                	<form role="form" action="" method="post" enctype="multipart/form-data">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Galerie modele</div>
                                            <div class="form-group col-lg-7">
                                            	<input type="file" name="loc_model[]" id="gallery" multiple required />
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="save_model" class="btn btn-danger">Upload</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </form>  
                                </div>
                                
                                <div class="tab-pane fade" id="contact">
                                	<form role="form" action="" method="post">
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Contact</div>
                                            <div class="form-group col-lg-7">
                                            	<p><input type="text" class="form-control" name="loc_phone" placeholder="Phone Number" required /></p>
                                                <p><input type="text" class="form-control" name="loc_website" placeholder="Website" required /></p>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="save_contact" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>		 
                                    </form>      
                                </div>
                                
                                <div class="tab-pane fade" id="joburi">
                                	<form role="form" action="" method="post">	 
                                    	<div class="col-lg-10">	
                                        	<div class="col-lg-3 label">Descriere</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobdesc" required></textarea>
                                            </div>
                                            <div class="col-lg-3 label">Cerinte</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobreq" required></textarea>
                                            </div>
                                            <div class="col-lg-3 label">Beneficii</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobbene" required></textarea>
                                            </div>
                                            <div class="col-lg-3 label">Responsabilitati</div>
                                            <div class="form-group col-lg-7">
                                            	<textarea class="form-control" rows="3" name="loc_jobresp" required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 pull-right">
                                        	<button type="submit" name="save_joburi" class="btn btn-danger">Salveaza</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>		 
                                    </form>      
                                </div>
                            </div>
                        </div>
                    	<div class="col-lg-7"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>