<div class="container">
	<div class="row">
    	<div class="col-md-4 col-md-offset-4">
        	<div class="login-panel panel panel-default">
            	<img style="padding-top:20px;margin:0 auto;display:block;" src="{$asset_url}/images/admin-logo.png" alt="Admin logo" />
                <div class="panel-body" style="padding:50px;">
                	<form action="" method="post" role="form">
                    	{if $error}<p style="text-align:center;">{$error}</p>{/if}
                    	<fieldset>
                            <div class="form-group">
                            	<input class="form-control" placeholder="email" name="email" id="email" type="email" value="{$remember_email}" required autofocus />
                            </div>
                            <div class="form-group">
                            	<input class="form-control" placeholder="parola" name="password" type="password" value="{$remember_pass}" required />
                            </div>
                            <div class="checkbox">
                            	<label>
                                	<input name="remember" type="checkbox" value="Remember Me" {if $remember_email && $remember_pass} checked="checked" {else} '' {/if} /> Tine-ma minte
                                </label>
                            </div>
                            <button type="submit" name="submit_login" id="submit_login" class="btn btn-lg btn-danger btn-block">Sign in</button><br />
                            <a href="{$base_url}/admin/forgot-password/">Ați uitat parola</a>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>