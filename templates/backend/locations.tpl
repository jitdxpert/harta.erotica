<div class="panel panel-red">
    <div class="panel-heading title-panel">
        Tabel locatii
    </div>
    <div class="panel-body">
        <div class="dataTable_wrapper">
        	{if $msg} {$msg} {/if}
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th width="2%">#</th>
                        <th width="15%">Nume</th>
                        <th width="22%">Descriere</th>
                        <th width="18%">Program</th>
                        <th width="8%">Plata</th>
                        <th width="15%">Locatie</th>
                        <th width="8%">Conturi</th>
                        <th width="5%">Stare</th>
                        <th class="text-center" width="7%">Acțiune</th>
                    </tr>
                </thead>
                <tbody>
                	{if $locations}
                        {section name=i loop=$locations}
                            <tr>
                                <td>{$locations[i].LID}</td>
                                <td>{$locations[i].loc_name}</td>
                                <td>{$locations[i].loc_desc|truncate:200}</td>
                                <td>
                                	LUNI: {$locations[i].loc_monday}<br />
                                    MARTI: {$locations[i].loc_tuesday}<br />
                                    MIERCURI: {$locations[i].loc_wednesday}<br />
                                    JOI: {$locations[i].loc_thursday}<br />
                                    VINERI: {$locations[i].loc_friday}<br />
                                    SAMBATA: {$locations[i].loc_saturday}<br />
                                    DUMINICA: {$locations[i].loc_sunday}
                                </td>
                                <td>{$locations[i].loc_payments}</td>
                                <td>{$locations[i].loc_address} / {$locations[i].loc_latitude} - {$locations[i].loc_longitude}</td>
                                <td>
                                	{assign var=loc_user value=get_username($locations[i].loc_user, 'name')}
                                    {$loc_user}
                                </td>
                                <td>
                                	{if $locations[i].loc_status=='hide'} 
	                                	Ascunde
                                    {else}
										Spectacol
                                    {/if}
                                </td>
                                <td class="text-center">
                                	<a style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="Edita" href="{$base_url}/admin/change-type/?type={$locations[i].loc_type}&LID={$locations[i].LID}&action=edit">
                                    	<i class="fa fa-edit"></i>
                                    </a>
                                    <a style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="Sterge" onclick="return confirm('Are you sure you want to delete this location?');" href="{$base_url}/admin/delete-location/?LID={$locations[i].LID}&action=delete">
                                    	<i class="fa fa-close"></i>
                                    </a>
                                </td>
                            </tr>
                        {/section}
                    {else}
                    	<tr>
                        	<td colspan="9">No locations are found</td>
                    	</tr>
                    {/if}
                </tbody>
            </table>
        </div>
    </div>
</div>