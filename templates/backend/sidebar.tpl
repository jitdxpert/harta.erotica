<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0;">
    	<div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{$base_url}/admin/">HARTA EROTICA</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a>{$AUNAME}</a></li>
                <li>
                    <a href="{$base_url}/admin/logout/"><i style="color:#d9534f;" class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
        </div>
	    <div class="navbar-default sidebar" role="navigation">
        	<div class="sidebar-nav navbar-collapse">
            	<ul class="nav" id="side-menu">
                	<li class="sidebar-search">
                    	<div class="input-group custom-search-form">
                        	<input type="text" class="form-control" placeholder="Cauta locatie/user.." />
                            <span class="input-group-btn">
                            	<button class="btn btn-danger" type="button">
	                                <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </li>
                    {if $AUROLE=='admin'}
	                	<li>
    	                	<a href="{$base_url}/admin/locations/"><i class="fa fa-map-marker fa-fw"></i> Locatii<span class="fa arrow"></span></a>
        	                <ul class="nav nav-second-level">
            	            	<li>
                	            	<a href="{$base_url}/admin/locations/"><i class="fa fa-th fa-fw"></i> Afiseaza</a>
                    	        </li>
                                <li>
                                    <a href="{$base_url}/admin/select-location/"><i class="fa fa-plus-circle fa-fw"></i> Adauga</a>
                                </li>
	                        </ul>
    	                </li>
                    {else}
                    	<li>
    	                	<a href="{$base_url}/admin/locations/"><i class="fa fa-map-marker fa-fw"></i> Locatii<span class="fa arrow"></span></a>
        	                <ul class="nav nav-second-level">
            	            	<li>
                	            	<a href="{$base_url}/admin/locations/"><i class="fa fa-th fa-fw"></i> Toate Locațiile</a>
                    	        </li>
                                {if $locations}
			                        {section name=i loop=$locations}
                                    	<li>
                                            <a href="{$base_url}/admin/change-type/?type={$locations[i].loc_type}&LID={$locations[i].LID}&action=edit"><i class="fa fa-map-marker fa-fw"></i> {$locations[i].loc_name}</a>
                                        </li>
                                    {/section}
                                {/if}
	                        </ul>
    	                </li>
                    {/if}
                    {if $AUROLE=='admin'}
	                    <li>
    	                	<a href="{$base_url}/admin/accounts/"><i class="fa fa-user fa-fw"></i> Conturi<span class="fa arrow"></span></a>
        	                <ul class="nav nav-second-level">
                                <li>
                                    <a href="{$base_url}/admin/accounts/"><i class="fa fa-th fa-fw"></i> Afiseaza</a>
                                </li>
                                <li>
                                    <a href="{$base_url}/admin/create-account/"><i class="fa fa-plus-circle fa-fw"></i> Adauga</a>
                                </li>
	                        </ul>
	                    </li>
                    {else}
                    	<li>
                            <a href="{$base_url}/admin/accounts/"><i class="fa fa-th fa-fw"></i> Conturi</a>
	                    </li>	
                    {/if}
                    
                    {if $AUROLE!='admin'}
                        <li>
                            <a href="{$base_url}/admin/contact/"><i class="fa fa-envelope fa-fw"></i> Contact</a>
                        </li>
                    {/if}
                </ul>
            </div>
        </div>
    </nav>
    
	<div id="page-wrapper">
    	<div class="row" style="padding-top:15px;">
        	<div class="col-lg-12">