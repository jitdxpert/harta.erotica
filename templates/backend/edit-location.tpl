<div id="page-wrapper-loc">
	<nav class="navbar navbar-bordeaux navbar-static-top" role="navigation" style="margin-bottom:0;">
    	<div class="navbar-header">
        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-links links-active" href="">INFORMATII <br>GENERALE</a>
            <a class="navbar-links outline-links" href="{$base_url}/admin/edit-location-info/?type={$loc_type}&LID={$LID}&action=edit">PAGINA DE<br>PREZENTARE</a>
        </div>
    </nav>
    
    <div class="row" style="padding-top:10px;">
    	<div class="col-lg-12">
        	{if $msg} <div class="col-lg-12">{$msg}</div> {/if}
        	<div class="panel">
            	<div class="panel-body">
                	<div class="row">
                    	<div class="col-lg-7">
                        	<form method="post" action="" role="form">
                            	<div class="col-lg-10">	
                                	<div class="col-lg-3 label">Tip locatie</div>
                                    <div class="form-group col-lg-7">
                                    	<select name="loc_tip" class="form-control" required>
                                        	<option {if $loc_tip=='Payed'} selected {/if} value="Payed">Payed</option>
                                            <option {if $loc_tip=='Free'} selected {/if} value="Free">Free</option>
                                        </select>
                                    </div>
                                </div>
                                {if $AUROLE=='admin'}
	                               	<div class="col-lg-10">	  
	                                	<div class="col-lg-3 label">Cont editare</div>
    	                                <div class="form-group col-lg-7">
                                            <select name="loc_user" class="form-control" required>
                                                {if $users}
                                                    {section name=i loop=$users}
                                                        <option {if $loc_user==$users[i].UID} selected {/if} value="{$users[i].UID}">{$users[i].email}</option>
                                                    {/section}
                                                {/if}
                                            </select>
	                                    </div>
    	                            </div>
                                {else}
                                    <input type="hidden" name="loc_user" value="{$AUID}" />
                                {/if}
                                <div class="col-lg-10">	
                                	<div class="col-lg-3 label">Locatie Verificata</div>
                                    <div class="form-group col-lg-7">
                                    	<select name="loc_verify" class="form-control" required>
                                        	<option {if $loc_verify=='Y'} selected {/if} value="Y">DA</option>
                                            <option {if $loc_verify=='N'} selected {/if} value="N">NU</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                	<div class="col-lg-3 label">Status locatie</div>
                                    <div class="form-group col-lg-7">
                                    	<select name="loc_status" class="form-control" required>
                                        	<option {if $loc_status=='show'} selected {/if} value="show">Publicata</option>
                                            <option {if $loc_status=='hide'} selected {/if} value="hide">Inactiva</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-10"> 
                                	<div class="col-lg-3 label">Contact 1</div>
                                    <div class="form-group col-lg-7">
                                    	<textarea name="loc_contact1" class="form-control" rows="3">{$loc_contact1}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-10">
                                	<div class="col-lg-3 label">Contact 2</div>
                                  	<div class="form-group col-lg-7">
                                    	<textarea name="loc_contact2" class="form-control" rows="3">{$loc_contact2}</textarea>
                                    </div>
                                </div>
                              	<div class="col-lg-10">	
                                	<div class="col-lg-3 label">Pagini active</div>
                                  	<div class="form-group col-lg-7">
                                    	{assign var=pages value=array('Profil', 'Servicii', 'Galerie locatie', 'Galerie Modele', 'Contact', 'Joburi')}
                                        {section name=i loop=$pages}
                                        	<div class="checkbox">
                                                <label>
                                                	{if $pages[i]|in_array:$loc_pages}
                                                        <input type="checkbox" value="{$pages[i]}" checked="checked" name="loc_pages[]" /> {$pages[i]}
                                                    {else}
                                                        <input type="checkbox" value="{$pages[i]}" name="loc_pages[]" /> {$pages[i]}
                                                    {/if}
                                                </label>
                                            </div>
                                        {/section}
                                    </div>
                                </div>
                                <div class="col-lg-12 pull-right">
                                	<button type="submit" name="update_location" class="btn btn-danger">Update</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>