<div class="panel panel-red">
	<div class="panel-heading title-panel">
    	Editare cont
    </div>
    <div class="panel-body">
    	<div class="row">
        	<div class="col-lg-7">
            	{if $msg} {$msg} {/if}
            	<form method="post" role="form" action="">
                	<div class="form-group col-md-12">
                    	<label><i class="fa fa-user"></i> Username</label>
                        <input class="form-control" id="name" name="name" value="{$name}" required /> 
                    </div>
                    <div class="form-group col-md-6">
                    	<label><i class="fa fa-lock"></i> Parola</label>
                        <input class="form-control" type="password" id="password" name="password" /> 
                    </div>
                    <div class="form-group col-md-6">
                    	<label><i class="fa fa-lock"></i> Confirma parola</label>
                        <input class="form-control" type="password" id="conpass" name="conpass" /> 
                    </div>
                    <div class="form-group col-md-12">
                    	<label><i class="fa fa-envelope"></i> Email</label>
                        <input class="form-control" type="email" id="email" name="email" value="{$email}" required /> 
                    </div>
                    <div class="form-group col-md-12">
                    	<label><i class="fa fa-info"></i> Informatii</label>
                        <textarea class="form-control" rows="3" id="info" name="info" required>{$info}</textarea>
                    </div>
                    <div class="col-md-12">
                    	<button type="submit" name="update_account" id="update_account" class="btn btn-danger">Actualizare</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>