<div class="panel panel-red">
	<div class="panel-heading title-panel">
    	Tabel conturi
    </div>
    
    <div class="panel-body">
    	<div class="dataTable_wrapper">
        	{if $msg} {$msg} {/if}
        	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
            	<thead>
                	<tr>
                    	<th width="2%">#</th>
                    	<th width="15%">User</th>
                        <th width="18%">Email</th>
                        <th width="30%">Informatii</th>
                        <th width="15%">Locatii</th>
                        <th width="5%">Stare</th>
                        <td class="text-center" width="5%">Acțiune</td>
                    </tr>
                </thead>
                <tbody>
                	{if $users}
                        {section name=i loop=$users}
                            <tr>
                            	<td>{$users[i].UID}</td>
                                <td>{$users[i].name}</td>
                                <td>{$users[i].email}</td>
                                <td>{$users[i].information}</td>
                                <td>
                                	{assign var=locations value=get_user_locations($users[i].UID)}
                                	{section name=n loop=$locations}
                                    	{$locations[n].loc_name}, 
                                    {/section}
                                </td>
                                <td> 
                                    {if $users[i].status==0}
                                        Dezactiveaza
                                    {else}
                                        Permite
                                    {/if}
                                </td>
                                <td class="text-center">
                                	<a style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="Edita" href="{$base_url}/admin/edit-account/?UID={$users[i].UID}&action=edit">
                                    	<i class="fa fa-edit"></i>
                                    </a>
                                    {if $users[i].UID!=1}
	                                    <a style="text-decoration:none;" data-toggle="tooltip" data-placement="top" title="Sterge" onclick="return confirm('Are you sure you want to delete this account?');" href="{$base_url}/admin/delete-account/?UID={$users[i].UID}&action=delete">
	                                    	<i class="fa fa-close"></i>
	                                    </a>
                                    {/if}
                                </td>
                            </tr>
                        {/section}
                    {else}
                    	<tr>
                        	<td colspan="7">No accounts are found</td>
                    	</tr>
                    {/if}
				</tbody>
            </table>
        </div>
    </div>
</div>