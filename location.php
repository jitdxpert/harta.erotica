<?php

define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';

$slug = explode('/', rtrim($_GET['location'], '/'));
if ( isset($slug[0]) ) {
	$LID = $slug[0];
}
if ( isset($slug[1]) ) {
	$loc_name = $slug[1];
}

if ( $LID && $loc_name ) {
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
	$rs = $conn->execute($query);
	$location = $rs->getrows();
	$location = $location[0];
	
	$page_title = $location['loc_name'] . ' - ' . $config['site_name'];

	$login = false;
	if (isset($_SESSION['UID']) && isset($_SESSION['UNAME']) && isset($_SESSION['UEMAIL']) && isset($_SESSION['UPSWD']) && isset($_SESSION['UROLE'])) {
		$smarty->assign('AUID', $_SESSION['AUID']);
		$smarty->assign('AUNAME', $_SESSION['AUNAME']);
		$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
		$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
		$smarty->assign('AUROLE', $_SESSION['AUROLE']);
		
		$login = true;
	}
	
	$smarty->assign('loggedin', $login);
	
	$smarty->assign('LID', $location['LID']);
	$smarty->assign('loc_type', $location['loc_type']);
	
	$smarty->assign('page_title', 		$page_title);
	$smarty->assign('page_keywords', 	$seo['home_keywords']);
	$smarty->assign('page_description', $seo['home_desc']);
	$smarty->assign('page_author', 		$seo['home_author']);
	
	$smarty->display('header.tpl');
	$smarty->display('location.tpl');
	$smarty->display('footer.tpl');
} else {
	SMRedirect::go($config['BASE_URL'] . '/404/');
}
?>