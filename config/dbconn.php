<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$conn = ADONewConnection($config['db_type']);
if (!$conn->Connect($config['db_host'], $config['db_user'], $config['db_pass'], $config['db_name'])) {
    echo 'Could not connect to mysql! Please check your database settings!';
    die();
}
$conn->execute("SET NAMES 'utf8'");
?>