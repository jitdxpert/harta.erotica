<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

define('DEBUG', TRUE);
ini_set('display_errors', 0);
if (DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}
?>