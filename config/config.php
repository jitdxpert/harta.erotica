<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

if ( function_exists('date_default_timezone_set') )
   date_default_timezone_set('Europe/Bucharest');

require dirname(__FILE__) . '/debug.php';
require dirname(__FILE__) . '/config.paths.php';
require dirname(__FILE__) . '/config.db.php';
require dirname(__FILE__) . '/config.local.php';
require dirname(__FILE__) . '/config.seo.php';

require $config['BASE_DIR']. '/classes/redirect.class.php';
require $config['BASE_DIR'] . '/config/security.php';
require $config['BASE_DIR'] . '/config/smarty/libs/Smarty.class.php';
require $config['BASE_DIR'] . '/config/adodb/adodb.inc.php';
require $config['BASE_DIR'] . '/config/dbconn.php';

if (!defined('_CONSOLE')) {
    require $config['BASE_DIR'] . '/config/sessions.php';
}

disableRegisterGlobals();

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 300);
ini_set('post_max_size', '256M');
ini_set('upload_max_filesize', '256M');
ini_set('max_file_uploads', 100);

require $config['BASE_DIR'] . '/config/smarty.php';
require $config['BASE_DIR'] . '/include/functions.php';
?>