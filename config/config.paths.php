<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$server = $_SERVER['HTTP_HOST'];
$config = array();
if ( $server == 'localhost' ) {
	$config['BASE_URL']   = 'http://' . $_SERVER['HTTP_HOST'] . '/harta.erotica';
} else {
	$config['BASE_URL']   = 'http://' . $_SERVER['HTTP_HOST'] . '/projects/harta.erotica';
}
$config['BASE_DIR']   = dirname(dirname(__FILE__));
$config['RELATIVE']   = '';
$config['ASSET_URL']  = $config['BASE_URL'] . '/assets';
$config['ASSET_DIR']  = $config['BASE_DIR'] . '/assets';
$config['IMG_URL']    = $config['ASSET_URL'] . '/images';
$config['IMG_DIR'] 	  = $config['ASSET_DIR'] . '/images';
$config['CSS_URL'] 	  = $config['ASSET_URL'] . '/css';
$config['CSS_DIR'] 	  = $config['ASSET_DIR'] . '/css';
$config['JS_URL'] 	  = $config['ASSET_URL'] . '/scripts';
$config['JS_DIR'] 	  = $config['ASSET_DIR'] . '/scripts';
$config['UPLOAD_URL'] = $config['BASE_URL'] . '/uploads';
$config['UPLOAD_DIR'] = $config['BASE_DIR'] . '/uploads';
$config['LOCATION_URL']  = $config['UPLOAD_URL'] . '/locations';
$config['LOCATION_DIR']  = $config['UPLOAD_DIR'] . '/locations';
$config['LOC_LOGO_URL']  = $config['LOCATION_URL'] . '/logos';
$config['LOC_LOGO_DIR']  = $config['LOCATION_DIR'] . '/logos';
$config['LOC_BANNER_URL']  = $config['LOCATION_URL'] . '/banners';
$config['LOC_BANNER_DIR']  = $config['LOCATION_DIR'] . '/banners';
$config['LOC_GALLERY_URL']  = $config['LOCATION_URL'] . '/galleries';
$config['LOC_GALLERY_DIR']  = $config['LOCATION_DIR'] . '/galleries';
$config['LOC_MODEL_URL']  = $config['LOCATION_URL'] . '/models';
$config['LOC_MODEL_DIR']  = $config['LOCATION_DIR'] . '/models';
?>