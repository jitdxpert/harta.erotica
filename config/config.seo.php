<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$seo 								= array();

// HOME Page
$seo['home_title'] 					= $config['site_name'];
$seo['home_keywords'] 				= '';
$seo['home_desc'] 					= '';
$seo['home_author'] 				= '';

// Admin FORGOT PASSWORD Page
$seo['admin_forgot_pass_title'] 		= 'Forgot Password - ' . $config['site_name'];
$seo['admin_forgot_pass_keywords'] 		= '';
$seo['admin_forgot_pass_desc'] 			= '';
$seo['admin_forgot_pass_author']		= '';

// Admin LOGIN Page
$seo['admin_login_title'] 			= 'Admin Login - ' . $config['site_name'];
$seo['admin_login_keywords'] 		= '';
$seo['admin_login_desc'] 			= '';
$seo['admin_login_author'] 			= '';

// ACCOUNTS Page
$seo['admin_dashboard_title'] 		= 'Dashboard - ' . $config['site_name'];
$seo['admin_dashboard_keywords'] 	= '';
$seo['admin_dashboard_desc'] 		= '';
$seo['admin_dashboard_author'] 		= '';

// ACCOUNTS Page
$seo['admin_accounts_title'] 		= 'Accounts - ' . $config['site_name'];
$seo['admin_accounts_keywords'] 	= '';
$seo['admin_accounts_desc'] 		= '';
$seo['admin_accounts_author'] 		= '';

// CREATE ACCOUNT Page
$seo['admin_create_account_title']		= 'Create Account - ' . $config['site_name'];
$seo['admin_create_account_keywords'] 	= '';
$seo['admin_create_account_desc'] 		= '';
$seo['admin_create_account_author']		= '';

// EDIT ACCOUNT Page
$seo['admin_edit_account_title']		= 'Edit Account - ' . $config['site_name'];
$seo['admin_edit_account_keywords'] 	= '';
$seo['admin_edit_account_desc'] 		= '';
$seo['admin_edit_account_author']		= '';


// LOCATIONS Page
$seo['admin_locations_title']		= 'Locations - ' . $config['site_name'];
$seo['admin_locations_keywords'] 	= '';
$seo['admin_locations_desc'] 		= '';
$seo['admin_locations_author']		= '';

// SELECT LOCATION Page
$seo['admin_select_location_title']		= 'Select Location - ' . $config['site_name'];
$seo['admin_select_location_keywords'] 	= '';
$seo['admin_select_location_desc'] 		= '';
$seo['admin_select_location_author']	= '';

// CREATE LOCATION Page
$seo['admin_create_location_title']		= 'Create Location - ' . $config['site_name'];
$seo['admin_create_location_keywords'] 	= '';
$seo['admin_create_location_desc'] 		= '';
$seo['admin_create_location_author']	= '';

// EDIT LOCATION Page
$seo['admin_edit_location_title']		= 'Edit Location - ' . $config['site_name'];
$seo['admin_edit_location_keywords'] 	= '';
$seo['admin_edit_location_desc'] 		= '';
$seo['admin_edit_location_author']		= '';


// CONTACT Page
$seo['admin_contact_title'] 		= 'Contact - ' . $config['site_name'];
$seo['admin_contact_keywords'] 	= '';
$seo['admin_contact_desc'] 		= '';
$seo['admin_contact_author'] 		= '';
?>