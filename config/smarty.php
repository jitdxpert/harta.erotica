<?php

if (!defined('_SMARTY_STARTED')) {
    include dirname(dirname(__FILE__)) . '/404.php';
    exit();
}

$tpl_dir = 'frontend';
if (defined('_ADMIN_STARTED')) {
    $tpl_dir = 'backend';
}

$smarty 				= new Smarty();
$smarty->compile_check 	= true;
$smarty->debugging 		= false;
$smarty->cache_dir		= $config['BASE_DIR'] . '/cache/' . $tpl_dir;
$smarty->template_dir 	= $config['BASE_DIR'] . '/templates/' . $tpl_dir;
$smarty->compile_dir 	= $config['BASE_DIR'] . '/cache/' . $tpl_dir;
foreach ($config as $key => $value) {
    $smarty->assign($key, $value);
}

$smarty->assign('base_url', 	$config['BASE_URL']);
$smarty->assign('base_dir', 	$config['BASE_DIR']);
$smarty->assign('relative', 	$config['RELATIVE']);
$smarty->assign('relative_tpl', $config['RELATIVE'] . '/templates/' . $tpl_dir);
$smarty->assign('asset_url', 	$config['ASSET_URL'] . '/' . $tpl_dir);
$smarty->assign('logo_url', 	$config['LOC_LOGO_URL']);
$smarty->assign('banner_url', 	$config['LOC_BANNER_URL']);
$smarty->assign('gallery_url', 	$config['LOC_GALLERY_URL']);
$smarty->assign('model_url', 	$config['LOC_MODEL_URL']);
$smarty->assign('ajax_url', 	$config['BASE_URL'] . '/ajax');
?>