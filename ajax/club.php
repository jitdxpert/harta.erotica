<?php
define('_SMARTY_STARTED', TRUE);
require_once dirname(dirname(__FILE__)) . '/config/config.php';

if ( isset($_GET) ) {
	$LID = $_GET['LID'];
	$type = $_GET['type'];
	
	$page = isset($_GET['page']);
	
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID AND `loc_type` = '$type' AND `loc_status` = 'show'";
	$rs  		= $conn->execute($query);
	if ( $rs ) {
		$locations 	= $rs->getrows();
	} else {
		echo 'Something went wrong, please try again.';
		return false;
	}
	$location 	= $locations[0];
	
	$loc_pages = explode(', ', $location['loc_pages']);
	
	$queryService 			= "SELECT * FROM `" . $config['db_prefix'] . "services` WHERE `LID` = $LID";
	$rsService 				= $conn->execute($queryService);
	if ( $rsService ) {
		$dataService 		= $rsService->getrows();
	} else {
		$dataService 		= array();
	}
	for ( $j = 0; $j < count($dataService); $j++ ) {
		$services[$j] = $dataService[$j]['service'];
		$durations[$j] = $dataService[$j]['duration'];
		$prices[$j] = $dataService[$j]['price'];
	} ?>
	
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 bhoechie-tab-menu">
                <ul class="nav nav-tabs tabs-left text-center">
                	<?php if ( in_array('Profil', $loc_pages) ) { ?>
                        <li class="club-profil active">
                            <a href="#club-profil" data-toggle="tab">
                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/profil/profil.png" />
                                <h4>Profil</h4>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ( in_array('Servicii', $loc_pages) ) { ?>
                        <li class="club-servicii">
                            <a href="#club-servicii" data-toggle="tab">
                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/profil/servicii.png" />
                                <h4>Servicii</h4>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ( in_array('Galerie locatie', $loc_pages) ) { ?>
                        <li class="club-galerie">
                            <a href="#club-galerie" data-toggle="tab">
                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/profil/galerie.png" />
                                <h4>Galerie</h4>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ( in_array('Galerie Modele', $loc_pages) ) { ?>
                        <li class="club-modele">
                            <a href="#club-modele" data-toggle="tab">
                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/profil/galerie.png" />
                                <h4>Modele</h4>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ( in_array('Contact', $loc_pages) ) { ?>
                        <li class="club-contact">
                            <a href="#club-contact" data-toggle="tab">
                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/profil/contact.png" />
                                <h4>Contact</h4>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ( in_array('Joburi', $loc_pages) ) { ?>
                        <li class="club-joburi">
                            <a href="#club-joburi" data-toggle="tab">
                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/profil/jobs.png" />
                                <h4>Joburi</h4>
                            </a>
                        </li>
                    <?php } ?>

                    <li style="position:absolute;bottom:0;left:10px;bottom:15px;">
                        <?php if ( $page == 'location' ) { ?>
                        	<a href="<?php echo $config['BASE_URL']; ?>">
                                <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
                            </a>
                        <?php } else { ?>
                            <a href="#">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </a>
                        <?php } ?>
                    </li>
                </ul>
            </div><!-- LEFT TAB HANDLE -->
            
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 bhoechie-tab pad-l-5">

                <div class="bhoechie-tab-content">
                    <div class="row profile-pic-map">
                        <div class="col-lg-12">
                            <img src="<?php echo $config['LOC_BANNER_URL']; ?>/<?php echo $location['loc_banners']; ?>" width="100%" height="140" />
                        </div>	
                    </div>
                    <div class="profile-info">
                    	<?php if ( $location['loc_verify'] == 'Y' ) { ?>
                            <div class="col-lg-1 verified-profile">
                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/verified.png">
                                <h6>LOCATIE VERIFICATA</h6>
                            </div>
                            <div class="col-lg-5 profile-name">
                        <?php } else { ?>
                        	<div class="col-lg-6 profile-name">
                        <?php } ?>
                            <h2 class="pull-left"><?php echo $location['loc_name']; ?></h2> 
                            <p class="open pull-right">
                            	<?php
								$CurDayName = date('l');
								$now = date("His");
								if ( $CurDayName == 'Monday' ) {
									$mon = explode('-', $location['loc_monday']);
									$start = date("His", strtotime($mon[0]));
									$end = date("His", strtotime($mon[1]));
									if($now >= $start && $now <= $end){
										echo "DESCHIS";
									}else{
										echo "închide";
									}
								} elseif ( $CurDayName == 'Tuesday' ) {
									$tue = explode('-', $location['loc_tuesday']);
									$start = date("His", strtotime($tue[0]));
									$end = date("His", strtotime($tue[1]));
									if($now >= $start && $now <= $end){
										echo "DESCHIS";
									}else{
										echo "închide";
									}
								} elseif ( $CurDayName == 'Wednesday' ) {
									$wed = explode('-', $location['loc_wednesday']);
									$start = date("His", strtotime($wed[0]));
									$end = date("His", strtotime($wed[1]));
									if($now >= $start && $now <= $end){
										echo "DESCHIS";
									}else{
										echo "închide";
									}
								} elseif ( $CurDayName == 'Thursday' ) {
									$thu = explode('-', $location['loc_thursday']);
									$start = date("His", strtotime($thu[0]));
									$end = date("His", strtotime($thu[1]));
									if($now >= $start && $now <= $end){
										echo "DESCHIS";
									}else{
										echo "închide";
									}
								} elseif ( $CurDayName == 'Friday' ) {
									$fri = explode('-', $location['loc_friday']);
									$start = date("His", strtotime($fri[0]));
									$end = date("His", strtotime($fri[1]));
									if($now >= $start && $now <= $end){
										echo "DESCHIS";
									}else{
										echo "închide";
									}
								} elseif ( $CurDayName == 'Saturday' ) {
									$sat = explode('-', $location['loc_saturday']);
									$start = date("His", strtotime($sat[0]));
									$end = date("His", strtotime($sat[1]));
									if($now >= $start && $now <= $end){
										echo "DESCHIS";
									}else{
										echo "închide";
									}
								} elseif ( $CurDayName == 'Sunday' ) {
									$sun = explode('-', $location['loc_sunday']);
									$start = date("His", strtotime($sun[0]));
									$end = date("His", strtotime($sun[1]));
									if($now >= $start && $now <= $end){
										echo "DESCHIS";
									}else{
										echo "închide";
									}
								}
								?>
                            </p>
                            <h6 class="pull-left"><i class="fa fa-phone"></i> <?php echo $location['loc_phone']; ?></h6> 
                            <h6 class="pull-left clear-left"><i class="fa fa-clock-o"></i> <?php echo $location['loc_program']; ?> </h6>
                        </div>
                        <div class="col-lg-2 profile-data border-left">
                            <a>
                                <h2><?php echo count(explode("|", $location['loc_galleries'])) + count(explode("|", $location['loc_models'])); ?>+</h2>
                                <p>poze</p>
                            </a>
                        </div>
                        <div class="col-lg-2 profile-data">
                            <h2 id="clubkm">0</h2>
                            <p>kilometri</p>
                        </div>
                        <div class="col-lg-2 profile-data no-border">
                            <h2><?php echo min($prices); ?><span>RON</span></h2>
                            <?php
							$key = array_search(min($prices), $prices);
							$text = $services[$key]; ?>
                            <p><?php echo $text; ?></p>
                        </div>
                    </div>	  
                </div><!-- TOP SECTION -->
                
                <div class="tab-content">
                    <?php if ( in_array('Profil', $loc_pages) ) { ?>
                        <div class="tab-pane active" id="club-profil">
                            <div class="row profile-boxes">
                                <div class="col-lg-6">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> DESCRIERE</h5>
                                        </div>
                                        <div class="nano">
                                            <div class="nano-content">
                                                <div class="profil-logo">
                                                    <img src="<?php echo $config['LOC_LOGO_URL']; ?>/<?php echo $location['loc_logo']; ?>" alt="logo" />
                                                </div>
                                                <div class="profil-desc">
                                                    <p><?php echo $location['loc_desc']; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row profil-footer">
                                            <a target="_blank" href="<?php echo $location['loc_website']; ?>"><?php echo $location['loc_website']; ?>
                                                <i class="fa  fa-arrow-circle-o-right pull-right"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> SERVICII</h5>
                                            <a href="#" data-rel="club-servicii" class="expand-sec">
                                                <i class="fa fa-external-link pull-right"></i>
                                            </a>
                                        </div>
                                        <div class="nano">
                                            <div class="nano-content">
                                                <div class="profil-desc">
                                                    <table class="table table-striped">
                                                        <?php for ( $i=0; $i<count($services); $i++ ) { ?>
                                                            <tr>
                                                                <td><?php echo $services[$i]; ?></td>
                                                                <td><?php echo $durations[$i]; ?></td>
                                                                <td><?php echo $prices[$i]; ?>RON</td>
                                                            </tr>
                                                        <?php } ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row profil-footer">
                                            <ul class="left">
                                                <?php 
                                                $payments = $location['loc_payments'];
                                                $payments = explode(", ", $payments);
                                                for ( $i=0; $i<count($payments); $i++ ) { ?>
                                                    <li>
                                                        <a data-toggle="tooltip" title="<?php echo $payments[$i]; ?>">
                                                            <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/light/<?php echo $payments[$i]; ?>.png" alt="" />
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                            <ul class="right">
                                                <?php 
                                                $filters = $location['loc_filters'];
                                                $filters = explode(", ", $filters);
                                                for ( $i=0; $i<count($filters); $i++ ) { ?>
                                                    <li>
                                                        <a data-toggle="tooltip" title="<?php echo $filters[$i]; ?>">
                                                            <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/dark/<?php echo $filters[$i]; ?>.png" alt="" />
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row profile-boxes">
                                <div class="col-lg-6">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> GALERIE MODELE</h5>
                                            <a href="#" data-rel="club-modele" class="expand-sec">
                                                <i class="fa fa-external-link pull-right"></i>
                                            </a>
                                        </div>
                                        <div class="nano">
                                            <div class="nano-content">
                                                <div class="profil-desc">
                                                    <div class="text-center">
                                                        <?php
                                                        $models = explode('|', $location['loc_model_visibility']);
                                                        foreach($models as $model) {
                                                            if ( strpos($model, 'hidden-') === false ) { ?>
                                                                <a href="#" data-rel="club-modele" class="expand-sec">
                                                                    <img class="gallery-thumb" src="<?php echo $config['LOC_MODEL_URL'] . '/thumb-' . $model; ?>" alt="thumb" />
                                                                </a>
                                                        <?php 
                                                            }
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row profil-footer">
                                            <p>Click pe poza pentru a deschide galeria</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> GALERIE LOCATIE</h5>
                                            <a href="#" data-rel="club-galerie" class="expand-sec">
                                                <i class="fa fa-external-link pull-right"></i>
                                            </a>
                                        </div>
                                        <div class="nano">
                                            <div class="nano-content">
                                                <div class="profil-desc">
                                                    <div class="text-center">
                                                        <?php
                                                        $galleries = explode('|', $location['loc_gallery_visibility']);
                                                        foreach($galleries as $gallery) {
                                                            if ( strpos($gallery, 'hidden-') === false ) { ?>
                                                                <a href="#" data-rel="club-galerie" class="expand-sec">
                                                                    <img class="gallery-thumb" src="<?php echo $config['LOC_GALLERY_URL'] . '/thumb-' . $gallery; ?>" alt="thumb" />
                                                                </a>
                                                        <?php 
                                                            }
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row profil-footer">
                                            <p>Click pe poza pentru a deschide galeria</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- PROFILE TAB -->
                    <?php } ?>
                    <?php if ( in_array('Servicii', $loc_pages) ) { ?>
                        <div class="tab-pane" id="club-servicii">
                            <div class="row profile-boxes">
                                <div class="col-lg-12">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> SERVICII SI PRETURI</h5>
                                        </div>
                                        <div class="row service-item text-center">
                                            <div class="col-lg-4">
                                                <h5>METODE DE PLATA</h5>
                                                <p><?php echo $location['loc_payments']; ?></p>
                                                <ul>
                                                    <?php 
                                                    $payments = $location['loc_payments'];
                                                    $payments = explode(", ", $payments);
                                                    for ( $i=0; $i<count($payments); $i++ ) { ?>
                                                        <li>
                                                            <a data-toggle="tooltip" title="<?php echo $payments[$i]; ?>">
                                                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/dark/<?php echo $payments[$i]; ?>.png" alt="" />
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                            <div class="col-lg-4">
                                                <h5>GRATUITATI</h5>
                                                <p><?php echo $location['loc_free']; ?></p>
                                            </div>
                                            <div class="col-lg-4">
                                                <h5>FACILITATI</h5>
                                                <p><?php echo $location['loc_filters']; ?></p>
                                                <ul>
                                                    <?php 
                                                    $filters = $location['loc_filters'];
                                                    $filters = explode(", ", $filters);
                                                    for ( $i=0; $i<count($filters); $i++ ) { ?>
                                                        <li>
                                                            <a data-toggle="tooltip" title="<?php echo $filters[$i]; ?>">
                                                                <img src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/images/icons/dark/<?php echo $filters[$i]; ?>.png" alt="" />
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="service-footer">
                                            <p><?php echo $location['loc_service_desc']; ?></p>
                                            <div class="col-lg-6 col-lg-offset-3">
                                                <table class="table table-striped">
                                                    <?php for ( $i=0; $i<count($services); $i++ ) { ?>
                                                        <tr>
                                                            <td><?php echo $services[$i]; ?></td>
                                                            <td><?php echo $durations[$i]; ?></td>
                                                            <td><?php echo $prices[$i]; ?>RON</td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- SERVICE TAB -->
                    <?php } ?>
                    <?php if ( in_array('Galerie locatie', $loc_pages) ) { ?>
                        <div class="tab-pane" id="club-galerie">
                            <div class="row profile-boxes">
                                <div class="col-lg-12">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> GALERIE LOCATIE</h5>
                                        </div>
                                        <div class="profil-desc">
                                            <div class="gallery-big text-center">
                                                <div id="club-gallery" class="flexslider">
                                                    <ul class="slides">
                                                        <?php
                                                        $galleries = explode('|', $location['loc_galleries']);
                                                        foreach($galleries as $gallery) { ?>
                                                            <li class="big-image"><img src="<?php echo $config['LOC_GALLERY_URL'] . '/big-' . $gallery; ?>" alt="gallery" /></li>
                                                        <?php 
                                                        } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="gallery-thumbs text-center">
                                                <div id="club-carousel" class="flexslider">
                                                    <ul class="slides">
                                                        <?php
                                                        $galleries = explode('|', $location['loc_galleries']);
                                                        foreach($galleries as $gallery) { ?>
                                                            <li class="galthumb"><img src="<?php echo $config['LOC_GALLERY_URL'] . '/thumb-' . $gallery; ?>" alt="thumb" /></li>
                                                        <?php 
                                                        } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- GALLERY TAB -->
                    <?php } ?>
                    <?php if ( in_array('Galerie Modele', $loc_pages) ) { ?>
                        <div class="tab-pane" id="club-modele">
                            <div class="row profile-boxes">
                                <div class="col-lg-12">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> GALERIE MODELE</h5>
                                        </div>
                                        <div class="profil-desc">
                                            <div class="gallery-big text-center">
                                                <div id="club-gallery-modele" class="flexslider">
                                                    <ul class="slides">
                                                        <?php
                                                        $models = explode('|', $location['loc_models']);
                                                        foreach($models as $model) { ?>
                                                            <li class="big-image"><img src="<?php echo $config['LOC_MODEL_URL'] . '/big-' . $model; ?>" alt="model" /></li>
                                                        <?php 
                                                        } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="gallery-thumbs text-center">
                                                <div id="club-carousel-modele" class="flexslider">
                                                    <ul class="slides">
                                                        <?php
                                                        $models = explode('|', $location['loc_models']);
                                                        foreach($models as $model) { ?>
                                                            <li class="galthumb"><img src="<?php echo $config['LOC_MODEL_URL'] . '/thumb-' . $model; ?>" alt="thumb" /></li>
                                                        <?php 
                                                        } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- MODELE TAB -->
                    <?php } ?>
                    <?php if ( in_array('Contact', $loc_pages) ) { ?>
                        <div class="tab-pane" id="club-contact">
                            <div class="row profile-boxes">
                                <div class="col-lg-12">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> CONTACT</h5>
                                        </div>
                                        <div class="profil-desc">
                                            <div id="club-contact-gmap" class="contact-map"></div>
                                        </div>
                                        <div class="profil-footer">
                                            <div class="col-lg-4">
                                                <img src="<?php echo $config['LOC_LOGO_URL']; ?>/<?php echo $location['loc_logo']; ?>" alt="logo" />
                                            </div>
                                            <div class="col-lg-4">
                                                <p><strong><i class="fa fa-phone"></i> TELEFON</strong><br />
                                                <?php echo $location['loc_phone']; ?></p>
                                                <p><strong><i class="fa fa-desktop"></i> WEBSITE</strong><br />
                                                <a target="_blank" href="<?php echo $location['loc_website']; ?>"><?php echo $location['loc_website']; ?></a></p>
                                            </div>
                                            <div class="col-lg-4">
                                                <p><strong><i class="fa fa-clock-o"></i> PROGRAM 
                                                <?php if ( $location['loc_deschis_inchide'] == 'open' ) { ?>
                                                    <span class="open pull-right">DESCHIS</span>
                                                <?php } elseif ( $location['loc_deschis_inchide'] == 'close' ) { ?>
                                                    <span class="open pull-right">închide</span>
                                                <?php } ?>
                                                </strong><br />
                                                <?php echo $location['loc_program']; ?></p>
                                                <p><strong><i class="fa fa-map-marker"></i> ADRESA</strong><br />
                                                <?php echo $location['loc_address']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6"></div>
                            </div>
                        </div><!-- CONTACT TAB -->
                    <?php } ?>
                    <?php if ( in_array('Joburi', $loc_pages) ) { ?>
                        <div class="tab-pane" id="club-joburi">
                            <div class="row profile-boxes">
                                <div class="col-lg-12">
                                    <div class="col-lg-12 profile-box">
                                        <div class="profil-head">
                                            <h5 class="pull-left"> ANGAJARI</h5>
                                        </div>
                                        <div class="joburi-item">
                                            <p><?php echo $location['loc_jobdesc']; ?></p>
                                            <table class="table table-striped">
                                                <tr>
                                                    <td>
                                                        <h6><i class="fa fa-question-circle"></i> CERINTE</h6>
                                                        <p><?php echo $location['loc_jobreq']; ?></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6><i class="fa fa-check-circle-o"></i> BENEFICII</h6>
                                                        <p><?php echo $location['loc_jobbene']; ?></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h6><i class="fa fa-thumbs-o-up"></i> RESPONSIBILITATI</h6>
                                                        <p><?php echo $location['loc_jobresp']; ?></p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- JOB TAB -->
                    <?php } ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <script src="<?php echo $config['ASSET_URL'] . '/' . $tpl_dir; ?>/scripts/modal-js.js" type="text/javascript"></script>
    <script type="text/javascript">
	$(function() {
		
		<?php if ( $page == 'location' ) { ?>
			$('a[data-dismiss="modal"]').click(function() {
				window.location.href = '<?php echo $config['BASE_URL']; ?>';
			});
		<?php } ?>
		
		$('.expand-sec').click(function() {
			var tab = $(this).attr('data-rel');
			$('.' + tab + ' > a').click();
			return false;
		});

		setTimeout(function() {
			$(".nano").nanoScroller();
		}, 500);
		
		setTimeout(function() {
			var user_lat = parseFloat($('.user_lat').val());
			var user_lng = parseFloat($('.user_lng').val());
			var loc_lat = parseFloat(<?php echo $location['loc_latitude']; ?>);
			var loc_lng = parseFloat(<?php echo $location['loc_longitude']; ?>);
			var origin = new google.maps.LatLng(user_lat, user_lng);
			var destination = new google.maps.LatLng(loc_lat, loc_lng);
			var service = new google.maps.DistanceMatrixService();
			service.getDistanceMatrix({
				origins: [origin],
				destinations: [destination],
				travelMode: google.maps.TravelMode.DRIVING,
				unitSystem: google.maps.UnitSystem.METRIC,
				avoidHighways: false,
				avoidTolls: false
			}, callback);
			function callback(response, status) {
				if (status != google.maps.DistanceMatrixStatus.OK) {
					alert('Error was: ' + status);
				} else {
					var origins = response.originAddresses;
					var destinations = response.destinationAddresses;
					var results = response.rows[0].elements;
					if ( results[0].status == 'ZERO_RESULTS' ) {
						$('#clubkm').html('N/A' + '<i class="fa fa-map-marker"></i>');
					} else {
						$('#clubkm').html(results[0].distance.text.replace('km', '') + '<i class="fa fa-map-marker"></i>');
					}
				}
			}
		}, 500);
		
		setTimeout(function() {
			ModalMapInit('club-gmap', <?php echo $location['loc_latitude']; ?>, <?php echo $location['loc_longitude']; ?>, 'images/marker-club.png');
		}, 500);
		
		$('a[href="#club-contact"]').click(function() {
			setTimeout(function() {
				ModalMapInit('club-contact-gmap', <?php echo $location['loc_latitude']; ?>, <?php echo $location['loc_longitude']; ?>);
			}, 500);
		});
		
		$('a[href="#club-galerie"]').click(function() {
			FlexSlider('#club-carousel', '#club-gallery');
		});
		
		$('a[href="#club-modele"]').click(function() {
			FlexSlider('#club-carousel-modele', '#club-gallery-modele');
		});
	});
	</script>
    
<?php 
} ?>