<?php
define('_SMARTY_STARTED', TRUE);
require_once dirname(dirname(__FILE__)) . '/config/config.php';

if ( isset($_REQUEST) ) {
	$type = $_REQUEST['type'];
	
	if ( isset($_REQUEST['keyword']) )
		$keyword = $_REQUEST['keyword'];

	if ( isset($_REQUEST['price']) )
		$price = $_REQUEST['price'];

	if ( isset($price) && !empty($price) ) {
		$prices = explode("-", $price);
		$start = $prices[0]; $end = $prices[1];
		$queryP = "SELECT `LID` FROM `" . $config['db_prefix'] . "services` WHERE `price` BETWEEN $start AND $end AND `type` = '$type'";
		$rsP	= $conn->execute($queryP);
		$services = $rsP->getrows();
		$LIDs = '';
		for ( $i = 0; $i < count($services); $i++ ) {
			$LIDs .= $services[$i]['LID'] . ', ';
		}
		$LIDs = substr($LIDs, 0, -2);
	}
	
	if ( isset($_REQUEST['facility']) )
		$facility = $_REQUEST['facility'];
	
	if ( isset($_REQUEST['user_lat']) && !empty($_REQUEST['user_lat']) )
		$user_lat = (float)$_REQUEST['user_lat'];
	else 
		$user_lat = 0;
	
	if ( isset($_REQUEST['user_lng']) && !empty($_REQUEST['user_lng']) )
		$user_lng = (float)$_REQUEST['user_lng'];
	else 
		$user_lng = 0;
		
	
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` IN ($LIDs) AND `loc_type` = '$type' AND `loc_status` = 'show'";
	if ( isset($keyword) && !empty($keyword) ) {
		$query .= " AND `loc_name` LIKE '$keyword%'";
	}
	if ( isset($facility) && !empty($facility) ) {
		$i = 0;
		$query .= " AND (";
		foreach($facility as $faci) {
			if ( $i == 0 ) {
				$query .= " `loc_filters` LIKE '%$faci%'";
			} else {
				$query .= " AND `loc_filters` LIKE '%$faci%'";
			}
			$i++;
		}
		$query .= ")";
	}
	$query .= " GROUP BY `LID`";
	
	$rs  		= $conn->execute($query);
	if ( $rs ) 
		$locations 	= $rs->getrows();
	else
		$locations = array();
		
	
	$data['type'] = $type;
	$data['query'] = $query;
	$data['count'] = count($locations);
	
	if ( count($locations) != 0 ) {
		if ( $type == 'salon' ) {
			$bgColor = 'bg-color-masaj';
			$color = 'color-masaj';
			$modallink = '#salonModal';
			$modalurl = $config['BASE_URL'] . '/ajax/salon.php';
			$marker = $config['ASSET_URL'] . '/' . $tpl_dir . '/images/pin-masaj.png';
		} elseif ( $type == 'shop' ) {
			$bgColor = 'bg-color-shop';
			$color = 'color-shop';
			$modallink = '#shopModal';
			$modalurl = $config['BASE_URL'] . '/ajax/shop.php';
			$marker = $config['ASSET_URL'] . '/' . $tpl_dir . '/images/pin-sexshop.png';
		} else {
			$bgColor = 'bg-color-night';
			$color = 'color-night';
			$modallink = '#clubModal';
			$modalurl = $config['BASE_URL'] . '/ajax/club.php';
			$marker = $config['ASSET_URL'] . '/' . $tpl_dir . '/images/pin-nightclub.png';
		}
	
		for ( $i = 0; $i < count($locations); $i++ ) {
			$queryService 			= "SELECT * FROM `" . $config['db_prefix'] . "services` WHERE `LID` = " . $locations[$i]['LID'];
			$rsService 				= $conn->execute($queryService);
			if ( $rsService ) {
				$dataService 		= $rsService->getrows();
			} else {
				$dataService 		= array();
			}
			for ( $j = 0; $j < count($dataService); $j++ ) {
				$services[$j] = $dataService[$j]['service'];
				$durations[$j] = $dataService[$j]['duration'];
				$prices[$j] = $dataService[$j]['price'];
			}
			$key = array_search(min($prices), $prices);
			$text = $services[$key];
			$filters = explode(", ", $locations[$i]['loc_filters']);
			
			$data['locations'][$i]["id"] = $locations[$i]['LID'];
			$data['locations'][$i]["name"] = $locations[$i]['loc_name'];
			$data['locations'][$i]["lat"] = $locations[$i]['loc_latitude'];
			$data['locations'][$i]["lng"] = $locations[$i]['loc_longitude'];
			$data['locations'][$i]["marker"] = $marker;
			$data['locations'][$i]["info"] = '<div class="infobox-left">
				<div class="small-info">
					<div class="distance-info">
						<h2 id="distance_map_' . $i . '" class="' . $color . '">0</h2>
						<span>kilometri</span>
						<p class="open">';
							$CurDayName = date('l');
							$now = date("His");
							if ( $CurDayName == 'Monday' ) {
								$mon = explode('-', $locations[$i]['loc_monday']);
								$start = date("His", strtotime($mon[0]));
								$end = date("His", strtotime($mon[1]));
								if($now >= $start && $now <= $end){
									$status = "DESCHIS";
								}else{
									$status = "închide";
								}
							} elseif ( $CurDayName == 'Tuesday' ) {
								$tue = explode('-', $locations[$i]['loc_tuesday']);
								$start = date("His", strtotime($tue[0]));
								$end = date("His", strtotime($tue[1]));
								if($now >= $start && $now <= $end){
									$status = "DESCHIS";
								}else{
									$status = "închide";
								}
							} elseif ( $CurDayName == 'Wednesday' ) {
								$wed = explode('-', $locations[$i]['loc_wednesday']);
								$start = date("His", strtotime($wed[0]));
								$end = date("His", strtotime($wed[1]));
								if($now >= $start && $now <= $end){
									$status = "DESCHIS";
								}else{
									$status = "închide";
								}
							} elseif ( $CurDayName == 'Thursday' ) {
								$thu = explode('-', $locations[$i]['loc_thursday']);
								$start = date("His", strtotime($thu[0]));
								$end = date("His", strtotime($thu[1]));
								if($now >= $start && $now <= $end){
									$status = "DESCHIS";
								}else{
									$status = "închide";
								}
							} elseif ( $CurDayName == 'Friday' ) {
								$fri = explode('-', $locations[$i]['loc_friday']);
								$start = date("His", strtotime($fri[0]));
								$end = date("His", strtotime($fri[1]));
								if($now >= $start && $now <= $end){
									$status = "DESCHIS";
								}else{
									$status = "închide";
								}
							} elseif ( $CurDayName == 'Saturday' ) {
								$sat = explode('-', $locations[$i]['loc_saturday']);
								$start = date("His", strtotime($sat[0]));
								$end = date("His", strtotime($sat[1]));
								if($now >= $start && $now <= $end){
									$status = "DESCHIS";
								}else{
									$status = "închide";
								}
							} elseif ( $CurDayName == 'Sunday' ) {
								$sun = explode('-', $locations[$i]['loc_sunday']);
								$start = date("His", strtotime($sun[0]));
								$end = date("His", strtotime($sun[1]));
								if($now >= $start && $now <= $end){
									$status = "DESCHIS";
								}else{
									$status = "închide";
								}
							}
						$data['locations'][$i]["info"] .= $status . '</p>
					</div>
					<div class="price-info">
						<h2 class="' . $color . '">' . min($prices) . '<span>RON</span></h2>
						<p>' . $text . '</p>
					</div>
				</div>
			</div>
			<div class="infobox-right">
				<div class="large-info">
					<h2 class="' . $color . '">' . $locations[$i]['loc_name'] . '</h2>
					<p>' . $locations[$i]['loc_address'] . '</p>';
					
					if ( count($filters) > 0 ) {
						$data['locations'][$i]["info"] .= '<ul class="list-inline">';
							foreach( $filters as $filter ) {
								$data['locations'][$i]["info"] .= '<li>
									<a data-toggle="tooltip" title="' . $filter . '">
										<img src="' . $config['ASSET_URL'] . '/' . $tpl_dir . '/images/icons/dark/' . $filter . '.png" />
									</a>
								</li>';
							}
						$data['locations'][$i]["info"] .= '</ul>';
					}
					$data['locations'][$i]["info"] .= '<p><i class="fa fa-clock-o ' . $color . '"></i> ' . $locations[$i]['loc_program'] . '</p>
					<p><i class="fa fa-phone ' . $color . '"></i> ' . $locations[$i]['loc_phone'] . '</p>
				</div>
			</div>
			<div class="infolink">
				<a data-rel="map" data-id="' . $locations[$i]['LID'] . '" data-type="' . $type . '" class="' . $bgColor . '" href="#" data-toggle="modal" data-target="' . $modallink . '"></a>
			</div>';
		}
		
	} else {
		$data['count'] = 0;
	}
	
	echo json_encode($data);
}