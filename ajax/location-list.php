<?php
define('_SMARTY_STARTED', TRUE);
require_once dirname(dirname(__FILE__)) . '/config/config.php';

if ( isset($_REQUEST) ) {
	$type = $_REQUEST['type'];
	
	if ( isset($_REQUEST['keyword']) )
		$keyword = $_REQUEST['keyword'];

	if ( isset($_REQUEST['price']) )
		$price = $_REQUEST['price'];

	if ( isset($price) && !empty($price) ) {
		$prices = explode("-", $price);
		$start = $prices[0]; $end = $prices[1];
		$queryP = "SELECT `LID` FROM `" . $config['db_prefix'] . "services` WHERE `price` BETWEEN $start AND $end AND `type` = '$type'";
		$rsP	= $conn->execute($queryP);
		$services = $rsP->getrows();
		$LIDs = '';
		for ( $i = 0; $i < count($services); $i++ ) {
			$LIDs .= $services[$i]['LID'] . ', ';
		}
		$LIDs = substr($LIDs, 0, -2);
	}
	
	if ( isset($_REQUEST['facility']) )
		$facility = $_REQUEST['facility'];
	
	if ( isset($_REQUEST['user_lat']) && !empty($_REQUEST['user_lat']) )
		$user_lat = (float)$_REQUEST['user_lat'];
	else 
		$user_lat = 0;
	
	if ( isset($_REQUEST['user_lng']) && !empty($_REQUEST['user_lng']) )
		$user_lng = (float)$_REQUEST['user_lng'];
	else 
		$user_lng = 0;
		
	
	$query = "SELECT * FROM `" . $config['db_prefix'] . "locations` WHERE `LID` IN ($LIDs) AND `loc_type` = '$type' AND `loc_status` = 'show'";
	if ( isset($keyword) && !empty($keyword) ) {
		$query .= " AND `loc_name` LIKE '$keyword%'";
	}
	if ( isset($facility) && !empty($facility) ) {
		$i = 0;
		$query .= " AND (";
		foreach($facility as $faci) {
			if ( $i == 0 ) {
				$query .= " `loc_filters` LIKE '%$faci%'";
			} else {
				$query .= " AND `loc_filters` LIKE '%$faci%'";
			}
			$i++;
		}
		$query .= ")";
	}
	$query .= " GROUP BY `LID`";
	
	$rs  		= $conn->execute($query);
	if ( $rs ) 
		$locations 	= $rs->getrows();
	else
		$locations = array();
	
	$data['type'] = $type;
	$data['query'] = $query;
	$data['count'] = count($locations);
	
	if ( count($locations) != 0 ) {
		if ( $type == 'salon' ) {
			$bgColor = 'bg-color-masaj';
			$color = 'color-masaj';
		} elseif ( $type == 'shop' ) {
			$bgColor = 'bg-color-shop';
			$color = 'color-shop';
		} else {
			$bgColor = 'bg-color-night';
			$color = 'color-night';
		}
		
		$data['html'] = '';
		for ( $i = 0; $i < count($locations); $i++ ) {
			$queryService 			= "SELECT * FROM `" . $config['db_prefix'] . "services` WHERE `LID` = " . $locations[$i]['LID'];
			$rsService 				= $conn->execute($queryService);
			if ( $rsService ) {
				$dataService 		= $rsService->getrows();
			} else {
				$dataService 		= array();
			}
			for ( $j = 0; $j < count($dataService); $j++ ) {
				$services[$j] = $dataService[$j]['service'];
				$durations[$j] = $dataService[$j]['duration'];
				$prices[$j] = $dataService[$j]['price'];
			}
			$key = array_search(min($prices), $prices);
			$text = $services[$key];
			$galleries = explode("|", $locations[$i]['loc_gallery_visibility']);
			$models = explode("|", $locations[$i]['loc_model_visibility']);
			$filters = explode(", ", $locations[$i]['loc_filters']);
			if ( $locations[$i]['loc_verify'] == 'Y' )
				$verified = 'verified';
			else
				$verified = '';
			
			$data['html'] .= '<div class="m-top-10">
				<div class="search-result ' . $verified . '">
					<div class="col-md-6 small-gallery">
						<div id="carousel-example-generic-' . $locations[$i]['LID'] . '" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner" role="listbox">';
								$data['html'] .= '<div class="item active">
										<img src="' . $config['LOC_LOGO_URL'] . '/list-' . $locations[$i]['loc_logo'] . '" width="300" height="181" />
									</div>';
								$n = 0;
								foreach ( $galleries as $image ) {
									if ( strpos($image, 'hidden-') === false ) {
										$data['html'] .= '<div class="item">
											<img src="' . $config['LOC_GALLERY_URL'] . '/list-' . $image . '" width="300" height="181" />
										</div>';
									}
									$n++;
								}
								foreach ( $models as $image ) {
									if ( strpos($image, 'hidden-') === false ) {
										$data['html'] .= '<div class="item">
											<img src="' . $config['LOC_MODEL_URL'] . '/list-' . $image . '" width="300" height="181" />
										</div>';
									}
									$n++;
								}
							$data['html'] .= '</div>
							<a class="left carousel-control ' . $bgColor . '" href="#carousel-example-generic-' . $locations[$i]['LID'] . '" role="button" data-slide="prev">
								<i class="fa fa-chevron-left" aria-hidden="true"></i>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control ' . $bgColor . '" href="#carousel-example-generic-' . $locations[$i]['LID'] . '" role="button" data-slide="next">
								<i class="fa fa-chevron-right" aria-hidden="true"></i>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
					<div class="col-md-2 small-info">
						<div class="distance-info">
							<h2 id="distance_list_' . $i . '" class="' . $color . '">0
								<script>CalcDistanceGoogleApi('.$locations[$i]['loc_latitude'].', '.$locations[$i]['loc_longitude'].', "#distance_list_' . $i . '");</script>
							</h2>
							<span>kilometri</span>
							<p class="open">';
								$CurDayName = date('l');
								$now = date("His");
								if ( $CurDayName == 'Monday' ) {
									$mon = explode('-', $locations[$i]['loc_monday']);
									$start = date("His", strtotime($mon[0]));
									$end = date("His", strtotime($mon[1]));
									if($now >= $start && $now <= $end){
										$status = "DESCHIS";
									}else{
										$status = "închide";
									}
								} elseif ( $CurDayName == 'Tuesday' ) {
									$tue = explode('-', $locations[$i]['loc_tuesday']);
									$start = date("His", strtotime($tue[0]));
									$end = date("His", strtotime($tue[1]));
									if($now >= $start && $now <= $end){
										$status = "DESCHIS";
									}else{
										$status = "închide";
									}
								} elseif ( $CurDayName == 'Wednesday' ) {
									$wed = explode('-', $locations[$i]['loc_wednesday']);
									$start = date("His", strtotime($wed[0]));
									$end = date("His", strtotime($wed[1]));
									if($now >= $start && $now <= $end){
										$status = "DESCHIS";
									}else{
										$status = "închide";
									}
								} elseif ( $CurDayName == 'Thursday' ) {
									$thu = explode('-', $locations[$i]['loc_thursday']);
									$start = date("His", strtotime($thu[0]));
									$end = date("His", strtotime($thu[1]));
									if($now >= $start && $now <= $end){
										$status = "DESCHIS";
									}else{
										$status = "închide";
									}
								} elseif ( $CurDayName == 'Friday' ) {
									$fri = explode('-', $locations[$i]['loc_friday']);
									$start = date("His", strtotime($fri[0]));
									$end = date("His", strtotime($fri[1]));
									if($now >= $start && $now <= $end){
										$status = "DESCHIS";
									}else{
										$status = "închide";
									}
								} elseif ( $CurDayName == 'Saturday' ) {
									$sat = explode('-', $locations[$i]['loc_saturday']);
									$start = date("His", strtotime($sat[0]));
									$end = date("His", strtotime($sat[1]));
									if($now >= $start && $now <= $end){
										$status = "DESCHIS";
									}else{
										$status = "închide";
									}
								} elseif ( $CurDayName == 'Sunday' ) {
									$sun = explode('-', $locations[$i]['loc_sunday']);
									$start = date("His", strtotime($sun[0]));
									$end = date("His", strtotime($sun[1]));
									if($now >= $start && $now <= $end){
										$status = "DESCHIS";
									}else{
										$status = "închide";
									}
								}
							$data['html'] .= $status . '</p>
						</div>
						<div class="price-info">
							<h2 class="' . $color . '">' . min($prices) . '<span>RON</span></h2>
							<p>' . $text . '</p>
						</div>
					</div>
					<div class="col-md-4 large-info">
						<h2>' . $locations[$i]['loc_name'] . '</h2>
						<p>' . $locations[$i]['loc_address'] . '</p>';
						if ( count($filters) > 0 ) {
							$data['html'] .= '<ul class="list-inline">';
								foreach( $filters as $filter ) {
									$data['html'] .= '<li>
										<a data-toggle="tooltip" title="' . $filter . '">
											<img src="' . $config['ASSET_URL'] . '/' . $tpl_dir . '/images/icons/dark/' . $filter . '.png" />
										</a>
									</li>';
								}
							$data['html'] .= '</ul>';
						}
						$data['html'] .= '<p><i class="fa fa-clock-o ' . $color . '"></i> ' . $locations[$i]['loc_program'] . '</p>
						<p style="padding-top:1px;padding-bottom:7px;"><i class="fa fa-phone ' . $color . '"></i> ' . $locations[$i]['loc_phone'] . '</p>
						<span class="' . $bgColor . '">
							<a data-type="' . $type . '" data-id="' . $locations[$i]['LID'] . '" class="ModalPush">
								VEZI LOCATIE <i class="fa fa-chevron-right pull-right"></i>
							</a>
						</span>
					</div>
				</div>
			</div>';
				
		}

	} else {
		$data['count'] = 0;
		$data['html'] = '<div class="m-top-10 col-lg-12 text-center" style="color:#fff;">No result found</div>';
	}
	
	echo json_encode($data);
}
?>