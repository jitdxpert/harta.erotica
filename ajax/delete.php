<?php
if (isset($_POST['type']) && isset($_POST['LID']) && isset($_POST['photo'])) {
    define('_SMARTY_STARTED', TRUE);
    define('_ADMIN_STARTED', TRUE);

    require_once dirname(dirname(__FILE__)) . '/config/config.php';
	
	$error = false;
	$type = trim($_POST['type']);
    $LID = trim($_POST['LID']);
    $photo = trim($_POST['photo']);
	
	if ( $type == 'gallery' ) {
		
	    $visibility = trim($_POST['visibility']);
		
		$select = "SELECT `loc_galleries`, `loc_gallery_visibility` FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
		$selectRS = $conn->execute($select);
		$rows = $selectRS->getrows();
		
		$GalleryPhotos = $rows[0]['loc_galleries'];
		$VisibilityPhotos = $rows[0]['loc_gallery_visibility'];
		
		$newGallery = str_ireplace($photo, '', $GalleryPhotos);
		$newGallery = str_ireplace('||', '|', $newGallery);
		$newGallery = rtrim($newGallery, '|');
        $newGallery = ltrim($newGallery, '|');
		
		$newVisibility = str_ireplace($visibility, '', $VisibilityPhotos);
		$newVisibility = str_ireplace('||', '|', $newVisibility);
		$newVisibility = rtrim($newVisibility, '|');
        $newVisibility = ltrim($newVisibility, '|');

		$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_galleries` = '$newGallery', `loc_gallery_visibility` = '$newVisibility' WHERE `LID` = $LID";
		$rs = $conn->execute($query);
		if ( $rs ) {
            @unlink($config['LOC_GALLERY_DIR'] . '/org-' . $photo);
			@unlink($config['LOC_GALLERY_DIR'] . '/thumb-' . $photo);
			@unlink($config['LOC_GALLERY_DIR'] . '/list-' . $photo);
			@unlink($config['LOC_GALLERY_DIR'] . '/big-' . $photo);
            @unlink($config['LOC_GALLERY_DIR'] . '/' . $photo);
        } else {
			$error = true;
		}
		
	} else if ( $type == 'model' ) {
		
	    $visibility = trim($_POST['visibility']);
		
		$select = "SELECT `loc_models`, `loc_model_visibility` FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
		$selectRS = $conn->execute($select);
		$rows = $selectRS->getrows();
		
		$ModelPhotos = $rows[0]['loc_models'];
		$VisibilityPhotos = $rows[0]['loc_model_visibility'];
		
		$newSnapshot = str_ireplace($photo, '', $ModelPhotos);
		$newSnapshot = str_ireplace('||', '|', $newSnapshot);
		$newSnapshot = rtrim($newSnapshot, '|');
        $newSnapshot = ltrim($newSnapshot, '|');
		
		$newVisibility = str_ireplace($visibility, '', $VisibilityPhotos);
		$newVisibility = str_ireplace('||', '|', $newVisibility);
		$newVisibility = rtrim($newVisibility, '|');
        $newVisibility = ltrim($newVisibility, '|');

		$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_models` = '$newSnapshot', `loc_model_visibility` = '$newVisibility' WHERE `LID` = $LID";
		$rs = $conn->execute($query);
		if ( $rs ) {
            @unlink($config['LOC_MODEL_DIR'] . '/org-' . $photo);
			@unlink($config['LOC_MODEL_DIR'] . '/thumb-' . $photo);
			@unlink($config['LOC_MODEL_DIR'] . '/list-' . $photo);
			@unlink($config['LOC_MODEL_DIR'] . '/big-' . $photo);
            @unlink($config['LOC_MODEL_DIR'] . '/' . $photo);
        } else {
			$error = true;
		}
		
	}
	
	if ( $error == true ) {
		echo 1;
	} else {
		echo 0;
	}
}
?>