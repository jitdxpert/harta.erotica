<?php
if (isset($_POST['type']) && isset($_POST['LID']) && isset($_POST['photo'])) {
    define('_SMARTY_STARTED', TRUE);
    define('_ADMIN_STARTED', TRUE);

    require_once dirname(dirname(__FILE__)) . '/config/config.php';
	
	$type = trim($_POST['type']);
    $LID = trim($_POST['LID']);
    $photo = trim($_POST['photo']);
	
	if ( $type == 'gallery' ) {
		
		$select = "SELECT `loc_gallery_visibility` FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
		$selectRS = $conn->execute($select);
		$rows = $selectRS->getrows();
		
		$gallery_visibility = $rows[0]['loc_gallery_visibility'];
		if ( strpos($photo, 'hidden-') !== false ) {
			$pic = str_ireplace('hidden-', '', $photo);
			$change = str_ireplace($photo, $pic, $gallery_visibility);
		} else {
			$pic = 'hidden-' . $photo;
			$change = str_ireplace($photo, $pic, $gallery_visibility);
		}
		$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_gallery_visibility` = '$change' WHERE `LID` = $LID";
		$rs = $conn->execute($query);
		
	} else if ( $type == 'model' ) {
		
		$select = "SELECT `loc_model_visibility` FROM `" . $config['db_prefix'] . "locations` WHERE `LID` = $LID LIMIT 0, 1";
		$selectRS = $conn->execute($select);
		$rows = $selectRS->getrows();
		
		$model_visibility = $rows[0]['loc_model_visibility'];
		if ( strpos($photo, 'hidden-') !== false ) {
			$pic = str_ireplace('hidden-', '', $photo);
			$change = str_ireplace($photo, $pic, $model_visibility);
		} else {
			$pic = 'hidden-' . $photo;
			$change = str_ireplace($photo, $pic, $model_visibility);
		}
		$query = "UPDATE `" . $config['db_prefix'] . "locations` SET `loc_model_visibility` = '$change' WHERE `MID` = $MID";
		$rs = $conn->execute($query);
	}
    
	if ( $pic ) {
		echo $pic;
	} else {
		echo 0;
	}
}
?>