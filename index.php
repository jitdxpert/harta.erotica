<?php

define('_SMARTY_STARTED', true);

require_once dirname(__FILE__) . '/config/config.php';

$error = NULL;

$login = false;
if (isset($_SESSION['UID']) && isset($_SESSION['UNAME']) && isset($_SESSION['UEMAIL']) && isset($_SESSION['UPSWD']) && isset($_SESSION['UROLE'])) {
    $smarty->assign('AUID', $_SESSION['AUID']);
	$smarty->assign('AUNAME', $_SESSION['AUNAME']);
	$smarty->assign('AUEMAIL', $_SESSION['AUEMAIL']);
	$smarty->assign('AUPSWD', $_SESSION['AUPSWD']);
	$smarty->assign('AUROLE', $_SESSION['AUROLE']);
	
	$login = true;
}

$smarty->assign('loggedin', $login);
$smarty->assign('error', $error);

$smarty->assign('page_title', 		$seo['home_title']);
$smarty->assign('page_keywords', 	$seo['home_keywords']);
$smarty->assign('page_description', $seo['home_desc']);
$smarty->assign('page_author', 		$seo['home_author']);

$smarty->display('header.tpl');
$smarty->display('index.tpl');
$smarty->display('footer.tpl');
?>