$(function() {
	
    $('#side-menu').metisMenu();
	$('.clockpicker').clockpicker();
	$('[data-toggle="tooltip"]').tooltip()
	
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});

$(document).ready(function() {
	var MaxInputs       = 100;
    var InputsWrapper   = $("#MoreServices");
	var ServiceRows		= $(".ServiceRows");
    var AddButton       = $("#add-service");

    var x = ServiceRows.length;
    var FieldCount = x - 1;

    $(AddButton).click(function () {
        if(x < MaxInputs) {
            FieldCount++;
            $(InputsWrapper).append('<div class="ServiceRows" id="ServiceRows_' + FieldCount + '">\
				<div class="col-lg-3 label">Servicii: <a href="javascript:void(0);" class="RemoveService pull-right">&times;</a></div>\
				<div class="form-group col-lg-7">\
					<input type="text" placeholder="Servicii"  class="form-control" name="loc_services[]" id="loc_services_' + FieldCount + '" />\
				</div>\
				<div class="col-lg-3 label">Durata:</div>\
				<div class="form-group col-lg-7">\
					<input type="text" placeholder="Durată"  class="form-control" name="loc_durations[]" id="loc_durations_' + FieldCount + '" />\
				</div>\
				<div class="col-lg-3 label">Preturi:</div>\
				<div class="form-group col-lg-7">\
					<input type="number" placeholder="Preturi"  class="form-control" name="loc_prices[]" id="loc_prices_' + FieldCount + '" />\
				</div>\
			</div>');
            x++;
        }
		
		$(".RemoveService").click(function(){
			$(this).parents('.ServiceRows').remove();
			x--;
			return false;
		});
        return false;
    });

    $(".RemoveService").click(function(){
		$(this).parents('.ServiceRows').remove();
        x--;
        return false;
    });
	
	
	
	$('#gallery-sortable').sortable({
        update: function() {
            var LID = $(this).attr("itemid");
            var stringDiv = "";
			var stringVisibility = "";
            $("#gallery-sortable").children().each(function(i) {
                var div = $(this);
                stringDiv += div.attr("itemid") + "|";
				stringVisibility += div.attr("data-id") + "|";
            });
            var dataString = "LID=" + LID + "&gallery=" + stringDiv + "&visibility=" + stringVisibility;
            $.ajax({
                type: "POST",
                url: base_url + "/ajax/sortable-gallery.php",
                data: dataString,
                success: function(data) {
                    $("#old_gallery").val(data);
                    $("#gallery-sortable").disableSelection();
                }
            });
        }
    });
	
	$('#model-sortable').sortable({
        update: function() {
            var LID = $(this).attr("itemid");
            var stringDiv = "";
			var stringVisibility = "";
            $("#model-sortable").children().each(function(i) {
                var div = $(this);
                stringDiv += div.attr("itemid") + "|";
				stringVisibility += div.attr("data-id") + "|";
            });
            var dataString = "LID=" + LID + "&model=" + stringDiv + "&visibility=" + stringVisibility;
            $.ajax({
                type: "POST",
                url: base_url + "/ajax/sortable-model.php",
                data: dataString,
                success: function(data) {
                    $("#old_model").val(data);
                    $("#model-sortable").disableSelection();
                }
            });
        }
    });
	
});


$(function() {
    $('.delete-cross').on('click', function() {
        var conf = confirm('Are you sure, you want to delete this photo?');
        if (conf) {
			var elem 	 = $(this);
			var type 	 = elem.attr('itemtype');
			var photo 	 = elem.attr('id');
			var visibility = elem.attr('data-id');
			var LID 	 = elem.parent().parent().attr('itemid');
			var dataString = "type=" + type + "&LID=" + LID + "&photo=" + photo + "&visibility=" + visibility;
			$.ajax({
				type: "POST",
				url: base_url + "/ajax/delete.php",
				data: dataString,
				success: function(data) {
					if ( data == 1 ) {
						alert('Something went wrong, please try again!');
					} else {
						elem.parent().remove();
					}
				}
			});
		}
	});
});

$(function() {
	$('.visibility-check').on('click', function() {
		var elem 	 = $(this);
		elem.html('<img src="' + base_url + '/assets/backend/dist/images/loader.gif" />');
		var type 	 = elem.attr('itemtype');
		var photo 	 = elem.attr('itemid');
		var LID 	 = elem.parent().parent().attr('itemid');
		var dataString = "type=" + type + "&LID=" + LID + "&photo=" + photo;
		$.ajax({
			type: "POST",
			url: base_url + "/ajax/visibility.php",
			data: dataString,
			success: function(data) {
				if ( data == 0 ) {
					alert('Something went wrong, please try again!');
				} else {
					if ( data.indexOf('hidden-') != -1 ) {
						elem.attr('itemid', data);
						elem.attr('title', 'Hidden');
						elem.css('background-position', '100% 0');
					} else {
						elem.attr('itemid', data);
						elem.attr('title', 'Visible');
						elem.css('background-position', '0 0');
					}
					elem.html('');
				} 
			}
		});
	});
});


var hash = window.location.hash;
$('#LocationTab a[href="' + hash + '"]').tab('show');