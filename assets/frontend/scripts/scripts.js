$('#gmap').css('width', $(window).width()-$('#sidebar-wrapper').width());

function GEOprocess(position) {
	$('.user_lat').val(position.coords.latitude);
	$('.user_lng').val(position.coords.longitude);
}
function GEOdeclined(error) {
	alert('Error: ' + error.message);
}

if ( navigator.geolocation ) {
	navigator.geolocation.getCurrentPosition(GEOprocess, GEOdeclined);
} else {
	alert('Your browser sucks. Upgrade it.');
}

// ** Map options settings ** //
var gmap = $('#gmap');
var center = new google.maps.LatLng(44.4268, 26.1025);
var mapOptions = {
	zoom				: 5,
	center				: center,
	panControl			: false,
	zoomControl			: false,
	scaleControl		: false,
	streetViewControl	: false,
	mapTypeId			: google.maps.MapTypeId.ROADMAP,
}

// ** Full page map init ** //
var markers = []; var infoBox = [];
var map = new google.maps.Map(gmap[0], mapOptions);
var markerCluster;

// ** Adding markers on map ** //
function GoogleMap(form) {
	$.ajax({
		type	 : "GET",
		dataType : "json",
		url		 : base_url + "/ajax/location-map.php",
		data	 : form,
		success: function(data) {
			infoBox.length = 0;
			deleteOverlays();
			if ( data.count > 0 ) {
				for ( var i = 0; i < data.locations.length; i++ ) {
					var dataLoc = data.locations[i];
					var clusterPin = dataLoc.marker;
					var markerPin = new google.maps.MarkerImage(dataLoc.marker, new google.maps.Size(29, 35));
					var marker = new google.maps.Marker({
						position : new google.maps.LatLng(dataLoc.lat, dataLoc.lng),
						map		 : map,
						icon	 : markerPin,
						lat	 	 : dataLoc.lat,
						lng		 : dataLoc.lng,
						title	 : dataLoc.name,
						id		 : dataLoc.id,
					});
					markers.push(marker);
					
					var infoOption = {
						content: '<div class="infobox"><div class="sidearrow"></div><div class="content">' + dataLoc.info + '</div></div>',
						disableAutoPan: false,
						maxWidth: 0,
						pixelOffset: new google.maps.Size(20, -68),
						zIndex: null,
						boxStyle: {
							background: "#ffffff",
							opacity: 1,
							width: "350px"
						},
						closeBoxMargin: "",
						closeBoxURL: "",
						infoBoxClearance: new google.maps.Size(1, 1),
						isHidden: false,
						pane: "floatPane",
						enableEventPropagation: false
					};
					var ib = new InfoBox(infoOption);
					infoBox.push(ib);
					google.maps.event.addListener(marker,'click',(function(marker, i) {
						return function() {
							close_popups();
							infoBox[i].open(map, this);
							CalcDistanceGoogleApi(marker.lat, marker.lng, '#distance_map_' + i);
						}
					})(marker, i));
				}
				
				map.setCenter(center);
			  	map.setZoom(5);
				
				var mcOptions = {
					styles: [{
						url: clusterPin,
						height: 35,
						width: 29,
						anchorIcon: [0, 0],
						fontFamily: 'Lato',
						textColor: 'white',
						textSize: 13,
					}]
				}
				markerCluster = new MarkerClusterer(map, markers, mcOptions);
			}
		},
		complete: function(){
			deleteOverlays();
		}
	});
}

// ** Load maps on page load with default salon type ** //
google.maps.event.addDomListener(window, 'load', function() {
	var form = $('#salon_form').serialize();
	GoogleMap(form);
});

// ** Left menu and map toggle functionality ** //
$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$(this).toggleClass('slide-in');
	$("#wrapper").toggleClass("toggled");
	if ( $('#wrapper').hasClass('toggled') ) {
		$('#gmap').css('width', '100%');
	} else {
		$('#gmap').css('width', $(window).width()-$('#sidebar-wrapper').width());
	}
	map.setCenter(center);
	map.setZoom(5);
});


function close_popups(){
	for(var i = 0; i<infoBox.length; i++){
		infoBox[i].close();
	}
}

// ** Remove all markers from map ** //
function deleteOverlays() {
    if (markers) {
		for (var i in markers) {
			markers[i].setMap(null);
		}
		markers.length = 0;
		markers = new Array();
	}
}


// ** Tooltip on hover ** //
$(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

// ** Function for generate location list ** //
function LocationList(form) {
	var formVal = form;
	formVal = formVal.split('&');
	var type = formVal[0].split('=');
	$('#' + type[1] + '_nano > .nano-content').html('<div class="m-top-10 col-lg-12 text-center"><img src="' + asset_url + '/images/loading.gif" alt="loading" /></div>');
	$.ajax({
		type	 : "GET",
		dataType : "json",
		url		 : base_url + "/ajax/location-list.php",
		data	 : form,
		success: function(data) {
			$('.nano').height($(window).height() - ($('.logo').height() + $('.nav-tabs').height() + $('.search-box').height() + 115));
			$('#' + type[1] + '_result').html('<h2>am gasit <strong>' + data.count + '</strong> de rezultate</h2>');
			$('#' + type[1] + '_nano > .nano-content').html(data.html);
			$(".nano").nanoScroller();
			$('[data-toggle="tooltip"]').tooltip();
			$('.ModalPush').click(function(){
				$('.modal-loading').removeClass('hide');
				var LID = $(this).attr('data-id');
				var type = $(this).attr('data-type');
				$.ajax({
					type : 'GET',
					url : base_url + '/ajax/' + type + '.php',
					data :  'LID='+ LID + '&type=' + type,
					success : function(data) {
						$('#' + type + 'Modal').modal('show');
						$('.modal .nav-tabs li:first > a').click();
						$('#' + type + 'Modal .modal-body').show().html(data);
						$('.modal-loading').addClass('hide');
					}
				});
				return false;
			});
		}
	});
}

// ** Load list on page load with default salon type ** //
window.onload = (function() {
	var form = $('#salon_form').serialize();
	LocationList(form);
});


// ** Search list slider ** //
function PriceSlider(selector) {
	$(selector).nstSlider({
		"left_grip_selector": ".leftGrip",
		"right_grip_selector": ".rightGrip",
		"value_bar_selector": ".innerBar",
		"rounding": {
			"5":"5", "10":"10", "15":"15", "20":"20", "25":"25", "30":"30", "35":"35", "40":"40", "45":"45", "50":"50", 
			"55":"55", "60":"60", "65":"65", "70":"70", "75":"75", "80":"80", "85":"85", "90":"90", "95":"95", "100":"100", 
			"105":"105", "110":"110", "115":"115", "120":"120", "125":"125", "130":"130", "135":"135", "140":"140", "145":"145", "150":"150", 
			"155":"155", "160":"160", "165":"165", "170":"170", "175":"175", "180":"180", "185":"185", "190":"190", "195":"195", "200":"200", 
			"205":"205", "210":"210", "215":"215", "220":"220", "225":"225", "230":"230", "235":"235", "240":"240", "245":"245", "250":"250", 
			"255":"255", "260":"260", "265":"265", "270":"270", "275":"275", "280":"280", "285":"285", "290":"290", "295":"295", "300":"300", 
			"305":"305", "310":"310", "315":"315", "320":"320", "325":"325", "330":"330", "335":"335", "340":"340", "345":"345", "350":"350", 
			"355":"355", "360":"360", "365":"365", "370":"370", "375":"375", "380":"380", "385":"385", "390":"390", "395":"395", "400":"400", 
			"405":"405", "410":"410", "415":"415", "420":"420", "425":"425", "430":"430", "435":"435", "440":"440", "445":"445", "450":"450", 
			"455":"455", "460":"460", "465":"465", "470":"470", "475":"475", "480":"480", "485":"485", "490":"490", "495":"495", "500":"500",
		},
		"highlight": {
			"grip_class": ".gripHighlighted",
			"panel_selector": ".highlightPanel"
		},
		"value_changed_callback": function(cause, leftValue, rightValue) {
			$(this).parents('.price').find('.leftLabel').text(leftValue);
			$(this).parents('.price').find('.rightLabel').text(rightValue);
			$(this).next('input[name="price"]').val(leftValue + '-' + rightValue);
		},
		"user_mouseup_callback": function(vmin, vmax, left_grip_moved) {
			$(this).parents('form').submit();
		}
	});
}
PriceSlider('#salon_price');
PriceSlider('#shop_price');
PriceSlider('#club_price');

// ** Search list custom scrollbar with dynamic height ** //
$('.nano').height($(window).height() - ($('.logo').height() + $('.nav-tabs').height() + $('.search-box').height() + 115 - 37));
$(".nano").nanoScroller();

// ** Left panel tab changes ** //
$('.tab_salon a, .tab_shop a, .tab_club a').click(function() {
	markerCluster.clearMarkers();
	deleteOverlays();
	$('.infobox').hide();
	var type = $(this).attr('aria-controls');
	$('#' + type + '_form')[0].reset();
	var form = $('#' + type + '_form').serialize();	
	LocationList(form);
	GoogleMap(form);
});

$('#sidebar-wrapper, #page-content-wrapper').click(function(e) {
    $('.infobox').hide();
});

$('.carousel').carousel({
  	interval: 1000
});


// ** Salon tab search ** //
$('#salon_form input[type="checkbox"]').click(function() {
	markerCluster.clearMarkers();
	deleteOverlays();
	$('#salon_form').submit();
});
$('#shop_form input[type="checkbox"]').click(function() {
	markerCluster.clearMarkers();
	deleteOverlays();
	$('#shop_form').submit();
});
$('#club_form input[type="checkbox"]').click(function() {
	markerCluster.clearMarkers();
	deleteOverlays();
	$('#club_form').submit();
});
$('#salon_form input[name="keyword"]').keyup(function() {
	markerCluster.clearMarkers();
	deleteOverlays();
	$('#salon_form').submit();
});
$('#shop_form input[name="keyword"]').keyup(function() {
	markerCluster.clearMarkers();
	deleteOverlays();
	$('#salon_form').submit();
});
$('#club_form input[name="keyword"]').keyup(function() {
	markerCluster.clearMarkers();
	deleteOverlays();
	$('#salon_form').submit();
});

$('#salon_form, #shop_form, #club_form').submit(function() {
	markerCluster.clearMarkers();
	var form = $(this).serialize();
	GoogleMap(form);
	LocationList(form);
	return false;
});


$("#salonModal, #shopModal, #clubModal").on("show.bs.modal", function(e) {
	var target = $(e.relatedTarget);
	var rel = target.attr('data-rel');
	if ( rel == 'map' ) {
		$('.modal-loading').removeClass('hide');
		var LID = target.attr('data-id');
		var type = target.attr('data-type');
		$.ajax({
			type : 'GET',
			url : base_url + '/ajax/' + type + '.php',
			data :  'LID='+ LID + '&type=' + type,
			success : function(data) {
				$('.modal .nav-tabs li:first > a').click();
				$('#' + type + 'Modal .modal-body').show().html(data);
				$('.modal-loading').addClass('hide');
			}
		});
	}
});


function CalcDistanceGoogleApi(lat, lng, elem) {
	var user_lat = parseFloat($('.user_lat').val());
	var user_lng = parseFloat($('.user_lng').val());
	var loc_lat = parseFloat(lat);
	var loc_lng = parseFloat(lng);
	var origin = new google.maps.LatLng(user_lat, user_lng);
	var destination = new google.maps.LatLng(loc_lat, loc_lng);
	var service = new google.maps.DistanceMatrixService();
	service.getDistanceMatrix({
		origins: [origin],
		destinations: [destination],
		travelMode: google.maps.TravelMode.DRIVING,
		unitSystem: google.maps.UnitSystem.METRIC,
		avoidHighways: false,
		avoidTolls: false
	}, callback);
	function callback(response, status) {
		if (status != google.maps.DistanceMatrixStatus.OK) {
			alert('Error was: ' + status);
		} else {
			var origins = response.originAddresses;
			var destinations = response.destinationAddresses;
			var results = response.rows[0].elements;
			if ( results[0].status == 'ZERO_RESULTS' ) {
				$(elem).html('N/A' + '<i class="fa fa-map-marker"></i>');
			} else {
				$(elem).html(results[0].distance.text.replace('km', '') + '<i class="fa fa-map-marker"></i>');
			}
		}
	}
}