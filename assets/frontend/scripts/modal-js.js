function ModalMapInit(elem, lat, lng, pin) {
	//if ( pin == false ) {
		var icon = new google.maps.MarkerImage(asset_url + '/images/small-marker.png', new google.maps.Size(30, 37));
	//} else {
		//var icon = new google.maps.MarkerImage(asset_url + '/' + pin, new google.maps.Size(132, 73));
	//}	
	var myLatlng = new google.maps.LatLng(lat, lng);
	var mapOptions = {
		zoom: 10,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		panControl: false,
		zoomControl: false,
		scaleControl: false,
	}
	var map = new google.maps.Map(document.getElementById(elem), mapOptions);
	
	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: icon,
	});
}


// ** Tooltip on hover ** //
$(function () {
	$('[data-toggle="tooltip"]').tooltip();
});


// ** Gallery on modal ** //
function FlexSlider(carousel, gallery) {
	$(carousel).flexslider({
		animation: "slide",
		controlNav: false,
		directionNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 64,
		itemMargin: 10,
		asNavFor: gallery
	});
	$(gallery).flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: carousel,
		start: function(slider){
			$('body').removeClass('loading');
		}
	});
}
$(function(){
	SyntaxHighlighter.all();
});