<?php

function distance($lat1, $lng1, $lat2, $lng2) {
	$gUrl = 'http://maps.googleapis.com/maps/api/distancematrix/json?origins='.$lat1.','.$lng1.'&destinations='.$lat2.','.$lng2.'&key=	`+++++++++++++++++++++++++++++++++++++++++++++++&mode=driving&units=metric&sensor=false';
	$gResponse = file_get_contents($gUrl);
	$gJSON = json_decode($gResponse);
	if ($gJSON->status == 'OK')
		if ( $gJSON->rows[0]->elements[0]->status == 'ZERO_RESULTS' )
			$DistanceInKM = 'N/A';
		else
	        $DistanceInKM = str_ireplace('km', '', $gJSON->rows[0]->elements[0]->distance->text);
	else
        $DistanceInKM = 'Error was: ' . $gJSON->status;
	
	return $DistanceInKM;
}

function get_username($uid, $field) {
	global $conn;
	global $config;
	$query = "SELECT `$field` FROM `" . $config['db_prefix'] . "users` WHERE `UID` = $uid";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$user = $rs->getrows();
		return $user[0][$field];
	}	
}

function get_user_locations($uid) {
	global $conn;
	global $config;
	$query = "SELECT `loc_name` FROM `" . $config['db_prefix'] . "locations` WHERE `loc_user` = $uid";
	$rs = $conn->execute($query);
	$num = $rs->numrows();
	if ( $num > 0 ) {
		$locations = $rs->getrows();
		return $locations;
	}
}