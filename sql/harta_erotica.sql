-- phpMyAdmin SQL Dump
-- version 4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 28, 2015 at 02:41 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `harta_erotica`
--

-- --------------------------------------------------------

--
-- Table structure for table `he_locations`
--

CREATE TABLE IF NOT EXISTS `he_locations` (
  `LID` int(11) NOT NULL,
  `loc_name` varchar(255) NOT NULL,
  `loc_type` enum('salon','shop','club') NOT NULL DEFAULT 'salon',
  `loc_tip` enum('Payed','Free') NOT NULL DEFAULT 'Payed',
  `loc_user` int(11) NOT NULL,
  `loc_banners` varchar(255) NOT NULL,
  `loc_logo` varchar(255) NOT NULL,
  `loc_galleries` longtext NOT NULL,
  `loc_gallery_visibility` longtext NOT NULL,
  `loc_models` longtext NOT NULL,
  `loc_model_visibility` longtext NOT NULL,
  `loc_desc` longtext NOT NULL,
  `loc_program` varchar(255) NOT NULL,
  `loc_monday` varchar(255) NOT NULL,
  `loc_tuesday` varchar(255) NOT NULL,
  `loc_wednesday` varchar(255) NOT NULL,
  `loc_thursday` varchar(255) NOT NULL,
  `loc_friday` varchar(255) NOT NULL,
  `loc_saturday` varchar(255) NOT NULL,
  `loc_sunday` varchar(255) NOT NULL,
  `loc_phone` varchar(255) NOT NULL,
  `loc_website` varchar(255) NOT NULL,
  `loc_payments` varchar(255) NOT NULL,
  `loc_latitude` float NOT NULL,
  `loc_longitude` float NOT NULL,
  `loc_address` varchar(255) NOT NULL,
  `loc_filters` varchar(255) NOT NULL,
  `loc_free` varchar(255) NOT NULL,
  `loc_service_desc` longtext NOT NULL,
  `loc_jobdesc` longtext NOT NULL,
  `loc_jobreq` longtext NOT NULL,
  `loc_jobbene` longtext NOT NULL,
  `loc_jobresp` longtext NOT NULL,
  `loc_deschis_inchide` enum('open','close') NOT NULL DEFAULT 'open',
  `loc_verify` enum('Y','N') NOT NULL DEFAULT 'N',
  `loc_status` enum('show','hide') NOT NULL DEFAULT 'show',
  `loc_contact1` longtext NOT NULL,
  `loc_contact2` longtext NOT NULL,
  `loc_pages` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `he_services`
--

CREATE TABLE IF NOT EXISTS `he_services` (
  `SID` int(11) NOT NULL,
  `LID` int(11) NOT NULL,
  `type` enum('salon','shop','club') NOT NULL,
  `service` longtext NOT NULL,
  `duration` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `he_users`
--

CREATE TABLE IF NOT EXISTS `he_users` (
  `UID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role` enum('user','admin') NOT NULL DEFAULT 'user',
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `information` longtext NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `he_users`
--

INSERT INTO `he_users` (`UID`, `name`, `role`, `email`, `password`, `information`, `status`, `created`) VALUES
(1, 'Administrator', 'admin', 'admin@harta.erotica.com', 'c984aed014aec7623a54f0591da07a85fd4b762d', 'Telefon : 0723231313 - Incasare plata pe data de 14 ', '1', '2015-01-14 04:27:14'),
(2, 'Surajit Pramanik', 'user', 'jitdxpert@gmail.com', 'c984aed014aec7623a54f0591da07a85fd4b762d', 'Telefon : 0723231313 - Incasare plata pe data de 14', '1', '2015-04-21 01:53:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `he_locations`
--
ALTER TABLE `he_locations`
  ADD PRIMARY KEY (`LID`);

--
-- Indexes for table `he_services`
--
ALTER TABLE `he_services`
  ADD PRIMARY KEY (`SID`);

--
-- Indexes for table `he_users`
--
ALTER TABLE `he_users`
  ADD PRIMARY KEY (`UID`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `he_locations`
--
ALTER TABLE `he_locations`
  MODIFY `LID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `he_services`
--
ALTER TABLE `he_services`
  MODIFY `SID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `he_users`
--
ALTER TABLE `he_users`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
